<?php
class MY_Controller extends CI_Controller {
	public $data = array();
	protected $main_layout = '';

	function __construct()
	{
		parent::__construct();
		
		$access_admin = $this->uri->segment(1);
		
		switch ($access_admin) {
			case 'admin' :
				{
					$this->load->helper('admin');
					$this->main_layout = 'admin/main';
					$flag_login = $this->session->userdata('login');
					
					if (! isset($flag_login)) {
						redirect(admin_url('login'));
					}
					$this->data['login'] = $this->session->userdata('login');
					
					break;
				}
			default :
				{
					// Danh sach danh muc san pham
					$this->load->model('catalog_model');
					$catalog_root = $this->catalog_model->get_list(array(
							'where' => array(
									'parent_id' => 0
							)
					));
					if (! empty($catalog_root) && count($catalog_root) > 0) {
						foreach ( $catalog_root as $cat ) {
							$subs = $this->catalog_model->get_list(array(
									'where' => array(
											'parent_id' => $cat->id
									)
							));
							$cat->subs = $subs;
						}
					}
					
					// Danh sach bai viet moi nhat
					$this->load->model('news_model');
					$news_last = $this->news_model->get_list(array(
							'order' => array(
									'created',
									'DESC'
							),
							'limit' => array(
									4,
									0
							)
					));
					
					//Gio hang
					$this->load->library('cart');				
					
					$this->data['total_items'] = $this->cart->total_items();
					$this->data['catalogs'] = $catalog_root;
					$this->data['news_last'] = $news_last;
				}
		}
	}
}
