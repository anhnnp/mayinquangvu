<?php
class Catalog_model extends MY_Model {
	function __construct(){
		parent::__construct();
		$this->table = 'catalog';
		$this->key = 'id';
	}
	
	public function get_parent($id){
		return $this->get_row_rule(array('id' => $id));
	}
}