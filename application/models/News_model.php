<?php
class News_model extends MY_Model {
	function __construct(){
		parent::__construct();
		$this->table = 'news';
		$this->key = 'id';
	}
}