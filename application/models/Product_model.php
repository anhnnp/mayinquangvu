<?php
class Product_model extends MY_Model {
	function __construct(){
		parent::__construct();
		$this->table = 'product';
		$this->key = 'id';
	}
}