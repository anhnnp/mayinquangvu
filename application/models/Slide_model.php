<?php
class Slide_model extends MY_Model {
	function __construct(){
		parent::__construct();
		$this->table = 'slide';
		$this->key = 'id';
	}
}