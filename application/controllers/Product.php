<?php
class Product extends MY_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('product_model');
	}
	
	function index(){
		
	}
	
	function catalog(){		
		$this->load->model('catalog_model');
		$this->load->library('pagination');
		
		$id_catalog = intval($this->uri->rsegment(3));
		
		$catalog = $this->catalog_model->get_row($id_catalog);
		if(!$catalog)
			redirect('home');
		
		// Kiem tra click vao danh muc cha hay danh muc con
		if($catalog->parent_id == 0){
			$catalog_subs = $this->catalog_model->get_list(array('where' => array('parent_id' => $catalog->id)));
			if(!empty($catalog_subs)){
				$id_catalog_subs = array();
				foreach ($catalog_subs as $cat){
					$id_catalog_subs[] = $cat->id;
				}
				$input['in'] = array('catalog_id', $id_catalog_subs);
			}			
		} else {
			$input['where'] = array('catalog_id' => $id_catalog);
		}
		
		$total = $this->product_model->get_totals($input);
		
		$config = array(
				'total_rows' => $total,
				'per_page' => 15,
				'uri_segment' => 4,
				'base_url' => base_url('product/catalog/'.$id_catalog),
				'next_link' => 'Trang kế tiếp',
				'prev_link' => 'Trang trước'
		);
		$this->pagination->initialize($config);
		$start_index = intval($this->uri->rsegment(4));
		$input['limit'] = array($config['per_page'], $start_index);
		
		$products = $this->product_model->get_list($input);		
		
		$this->data['total'] = $total;
		$this->data['catalog'] = $catalog;
		$this->data['products'] = $products;
		$this->data['temp'] = 'site/product/catalog';
		$this->load->view('site/layout', $this->data);
	}
	
	function view(){
		$id_product = intval($this->uri->rsegment(3));
		$product = $this->product_model->get_row($id_product);
		if(!$product) redirect(base_url());
		
		$this->load->model('catalog_model');
		$catalog = $this->catalog_model->get_row($product->catalog_id);
		
		if(!empty($product->image_list))
			$this->data['image_list'] = json_decode($product->image_list);	
		
		$count_view = $product->view + 1;
		$this->product_model->update($id_product, array('view' => $count_view));
		
		$this->data['product'] = $product;
		$this->data['catalog'] = $catalog;
		$this->data['temp'] = 'site/product/view';
		$this->load->view('site/layout', $this->data);
	}
	
	function search(){
		$search_type = $this->uri->rsegment(3);
		$this->load->model('product_model');
		
		switch($search_type){
			case 'name': {
				$this->load->library('pagination');
				
				$key_search = $this->input->get('key-search');
				
				$input = array('like' => array('name', $key_search));
				$total = $this->product_model->get_totals($input);
				
				$this->pagination->initialize(array(
						'total_rows' => $total,
						'base_url' => base_url('product/search/name'),
						'per_page' => 15,
						'uri_segment' => 4,
						'prev_link' => 'Trang trước',
						'next_link' => 'Trang kế tiếp'
				));
				
				$start_index_page = intval($this->uri->rsegment(4));
				
				$input['limit'] = array(15, $start_index_page);
				
				$list = $this->product_model->get_list($input);
				
				$this->data['products'] = $list;
				$this->data['total'] = $total;
				$this->data['temp'] = 'site/product/search';
				$this->load->view('site/layout', $this->data);
			} break;
				
			case 'price': {
				$this->load->library('pagination');
				
				$price_from = $this->input->get('price_from');
				$price_to = $this->input->get('price_to');
				
				$input = array('where' => array('price <=' => $price_to, 'price >=' => $price_from));
				$total = $this->product_model->get_totals($input);
				
				$this->pagination->initialize(array(
						'total_rows' => $total,
						'base_url' => base_url('product/search/name'),
						'per_page' => 15,
						'uri_segment' => 4,
						'prev_link' => 'Trang trước',
						'next_link' => 'Trang kế tiếp'
				));
				
				$start_index_page = intval($this->uri->rsegment(4));
				
				$input['limit'] = array(15, $start_index_page);
				
				$list = $this->product_model->get_list($input);
				
				$this->data['products'] = $list;
				$this->data['total'] = $total;
				$this->data['temp'] = 'site/product/search';
				$this->load->view('site/layout', $this->data);
			}  break;
			
			case 'show': {
				$key_search = $this->input->get('term');
				
				$input = array('like' => array('name', $key_search));
				
				$input['limit'] = array(5, 0);
				
				$list = $this->product_model->get_list($input);
				
				$product_name = array();
				foreach($list as $row){
					$product_name[] = $row->name;
				}
				echo json_encode($product_name);
				exit();
			} break;
			
			default: redirect(base_url());
		}
	}
}