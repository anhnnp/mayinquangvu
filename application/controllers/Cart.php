<?php
class Cart extends MY_Controller {
	function __construct() {
		parent::__construct();
		
	}
	
	public function index(){
		$cart = $this->cart->contents();
		
		$this->data['cart'] = $cart;
		$this->data['temp'] = 'site/cart/index';
		$this->load->view('site/layout', $this->data);
	}
	
	public function add(){
		$this->load->model('product_model');
		$id_product = intval($this->uri->rsegment(3));
		$product = $this->product_model->get_row($id_product);
		if(!$product) redirect(base_url());
		
		$this->cart->insert(array(
				'id' => $product->id,
				'qty' => 1,
				'price' => $product->price,
				'name' => $product->name,
				'image_link' => $product->image_link
		));
		
		redirect(base_url('/cart/'));
	}
	
	public function update(){
		if($this->input->post()){
			$cart = $this->cart->contents();
			$data = array();
			foreach($cart as $k => $c){
				$qty = $this->input->post('qty_'.$c['id']);
				$data[] = array('rowid' => $k, 'qty' => $qty);
			}
			$this->cart->update($data);
			redirect(base_url('/cart/'));
		}
	}
	
	public function delete(){
		$id_cart = intval($this->uri->rsegment(3));
		
		if(!empty($id_cart) && $id_cart > 0){
			$cart = $this->cart->contents();
			foreach($cart as $k => $c){
				if($c['id'] == $id_cart){
					$this->cart->update(array('rowid' => $k, 'qty' => 0));
					break;
				}
			}
			
		} else {
			$this->cart->destroy();
		}
		redirect(base_url('/cart/'));
	}
}