<?php
class Order extends MY_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('order_model');
		$this->load->model('transaction_model');
	}
	
	function checkout(){
		$cart = $this->cart->contents();
		if(!$cart) redirect();
		
		$user = NULL;
		if($user_login = $this->session->userdata('user_login')){
			$user = $this->user_model->get_row($user_login['id']);			
		}
		
		$this->load->library('form_validation');
		$this->load->helper('form');		
		
		if($this->input->post()){
			$this->form_validation->set_rules('name', 'họ và tên', 'required');
			$this->form_validation->set_rules('email', 'địa chỉ hòm thư', 'required|valid_email');
			$this->form_validation->set_rules('phone', 'số điện thoại', 'required|is_natural');
			$this->form_validation->set_rules('payment', 'phương thức thanh toán', 'required');
			
			if($this->form_validation->run()){
				$user_id = $user?$user->id:0;
				$user_name = $this->input->post('name');
				$user_email = $this->input->post('email');
				$user_phone = $this->input->post('phone');
				$message = $this->input->post('message');
				$payment = $this->input->post('payment');
				
				$amount = 0;
				foreach($cart as $p){
					$amount += $p['subtotal'];
				}
				
				$data = array(
						'status' => 0,
						'user_id' => $user_id,
						'user_name' => $user_name,
						'user_email' => $user_email,
						'user_phone' => $user_phone,
						'amount' => $amount,
						'message' => $message,
						'payment' => $payment,
						'created' => time()
				);
				
				if($this->transaction_model->create($data)){
					$transaction_id = $this->db->insert_id();
					foreach ($cart as $p){
						$this->order_model->create(array(
								'transaction_id' => $transaction_id,
								'product_id' => $p['id'],
								'qty' => $p['qty'],
								'amount' => $p['subtotal'],
								'status' => 0
						));
					}
					$this->cart->destroy();
					redirect();
				}
			}			
		}
		
		$this->data['user'] = $user;
		$this->data['temp'] = 'site/order/checkout';
		$this->load->view('site/layout', $this->data);
	}
}