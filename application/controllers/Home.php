<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends MY_Controller {

    public function index() {
    	
    	//Load slide
    	$this->load->model('slide_model');
    	$slides = $this->slide_model->get_list(array('order' => array('sort_order', 'ASC')));
    	
    	//Load san pham
    	$this->load->model('product_model');
    	$products_last = $this->product_model->get_list(array(
    			'order' => array('created', 'DESC'),
    			'limit' => array(3, 0)
    	));
    	$products_discount = $this->product_model->get_list(array(
    			'order' => array('discount', 'DESC'),
    			'limit' => array(3, 0)
    	));
    	$count_view = $this->product_model->get_list(array(
    			'order' => array('view', 'DESC'),
    			'limit' => array(3, 0)
    	));
    	
    	$this->data['slides'] = $slides;
    	$this->data['products_last'] = $products_last;
    	$this->data['products_discount'] = $products_discount;
    	$this->data['count_view'] = $count_view;
        $this->data['temp'] = 'site/home/index';
        $this->load->view('site/layout', $this->data);
    }

}
