<?php
class Upload extends MY_Controller {
	function __construct(){
		parent::__construct();		
	}
	
	public function index(){
		if($this->input->post('submit')){
			$this->load->library('upload_library');
			$path_file = './upload/user';
			$data = $this->upload_library->upload_file($path_file, 'userfile');
		}
		
		$this->data['temp'] = 'admin/upload/index';
		$this->load->view($this->main_layout, $this->data);
	}
	
	public function upload_files(){
		if($this->input->post('submit')){
			$this->load->library('upload_library');
			$path_file = './upload/user';
			$data = $this->upload_library->upload_files($path_file, 'images');
			show_text($data, 1);
		}
				
		$this->data['temp'] = 'admin/upload/upload_files';
		$this->load->view($this->main_layout, $this->data);
	}
}