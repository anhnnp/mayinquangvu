<?php
class Product extends MY_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('product_model');
	}

	public function index()
	{
		$this->load->helper('date_helper');
		// Phan trang
		$this->load->library('pagination');
		$input = array(
				'select' => 'id'
		);
		$this->__filter_query($input);
		$totals = $this->product_model->get_totals($input);
		$config = array(
				'total_rows' => $totals,
				'base_url' => admin_url('product/index'),
				'per_page' => 5,
				'uri_segment' => 4,
				'num_links' => 2,
				'reuse_query_string' => TRUE,
				'use_page_numbers' => TRUE,
				'next_link' => 'Trang kế tiếp',
				'prev_link' => 'Trang trước'
		);
		$this->pagination->initialize($config);
		
		$start_index = $this->uri->segment(4);
		if (! $start_index && ! is_int($start_index)) {
			$start_index = 0;
		} else {
			$start_index = ($start_index-1)*$config['per_page'];
		}
		
		$input = array(
				'limit' => array(
						$config['per_page'],
						$start_index
				)
		);
		
		// Loc du lieu neu co
		$this->__filter_query($input);
		
		$list = $this->product_model->get_list($input);
		
		// Danh sach danh muc
		$this->load->model('catalog_model');
		$input = array(
				'where' => array(
						'parent_id' => '0'
				)
		);
		$catalogs = $this->catalog_model->get_list($input);
		foreach ( $catalogs as $c ) {
			$input = array(
					'where' => array(
							'parent_id' => $c->id
					)
			);
			$sub_catalogs = $this->catalog_model->get_list($input);
			$c->sub = $sub_catalogs;
		}
		
		$this->data['message'] = $this->session->flashdata('message');
		$this->data['total'] = $totals;
		$this->data['catalogs'] = $catalogs;
		$this->data['list'] = $list;
		$this->data['temp'] = 'admin/product/index';
		$this->load->view($this->main_layout, $this->data);
	}

	private function __filter_query(&$input)
	{
		$id_filter = $this->input->get('id');
		$id_filter = intval($id_filter);
		if ($id_filter > 0){
			$input['where']['id'] = $id_filter;
		}
		$name_filter = $this->input->get('name');
		if (! empty($name_filter)){
			$input['like'][] = 'name';
			$input['like'][] = $name_filter;
		}
		$catalog_filter = $this->input->get('catalog');
		$catalog_filter = intval($catalog_filter);
		if (! empty($catalog_filter) && $catalog_filter > 0)
			$input['where']['catalog_id'] = $catalog_filter;
		return $input;
	}

	public function add()
	{
		$this->load->library('form_validation');
		$this->load->helper('form');
		
		if($this->input->post()){
			$this->form_validation->set_rules('name', 'tên sản phẩm', 'required|max_length[255]');						
			
			if($this->form_validation->run()){
				$name = $this->input->post('name');
				$price = str_replace(',', '', $this->input->post('price'));
				$discount = $this->input->post('discount');
				$catalog = $this->input->post('catalog');
				$warranty = $this->input->post('warranty');
				$gift = $this->input->post('gift');
				$site_title = $this->input->post('site_title');
				$meta_key = $this->input->post('meta_key');
				$meta_desc = $this->input->post('meta_desc');
				$content = $this->input->post('content');							
				
				$data = array(
						'name' => $name,
						'price' => $price,
						'discount' => $discount,
						'catalog_id' => $catalog,
						'warranty' => $warranty,
						'gifts' => $gift,
						'site_title' => $site_title,
						'meta_key' => $meta_key,
						'meta_desc' => $meta_desc,
						'created' => time(),
						'content' => $content
				);
				
				$this->load->library('upload_library');
				$path_file = './upload/product';
				$image_data = $this->upload_library->upload_file($path_file, 'image');
				if(!empty($image_data['file_name'])){
					$data['image_link'] = $image_data['file_name'];
				}
				
				$image_list = $this->upload_library->upload_files($path_file, 'image_list');
				foreach($image_list as $image){
					if(!empty($image['file_name'])){		
						$data['image_list'][] = $image['file_name'];
					}
				}
				if(!empty($data['image_list']) && count($data['image_list']) > 0)
					$data['image_list'] = json_encode($data['image_list']);
				
				if($this->product_model->create($data)){
					$this->session->set_flashdata('message', 'Thêm mới sản phẩm thành công');
				} else {
					$this->session->set_flashdata('message', 'Chưa thêm được sản phẩm');
				}
				redirect(admin_url('product'));				
			}
		}
		
		// Danh sach danh muc
		$this->load->model('catalog_model');
		$input = array(
				'where' => array(
						'parent_id' => '0'
				)
		);
		$catalogs = $this->catalog_model->get_list($input);
		foreach ( $catalogs as $c ) {
			$input = array(
					'where' => array(
							'parent_id' => $c->id
					)
			);
			$sub_catalogs = $this->catalog_model->get_list($input);
			$c->sub = $sub_catalogs;
		}
		
		$this->data['catalogs'] = $catalogs; 
		$this->data['temp'] = 'admin/product/add';
		$this->load->view($this->main_layout, $this->data);
	}
	
	public function edit(){
		$this->load->library('form_validation');
		$this->load->helper('form');
		
		$product_id = intval($this->uri->segment('4'));
		$product = $this->product_model->get_row($product_id);
		if(!$product){
			$this->session->set_flashdata('message', 'Không tồn tại sản phẩm này');
			redirect(admin_url('product'));
		}
		
		if($this->input->post()){
			$this->form_validation->set_rules('name', 'tên sản phẩm', 'required|max_length[255]');
				
			if($this->form_validation->run()){
				$name = $this->input->post('name');
				$price = str_replace(',', '', $this->input->post('price'));
				$discount = str_replace(',', '', $this->input->post('discount'));
				$catalog = $this->input->post('catalog');
				$warranty = $this->input->post('warranty');
				$gift = $this->input->post('gift');
				$site_title = $this->input->post('site_title');
				$meta_key = $this->input->post('meta_key');
				$meta_desc = $this->input->post('meta_desc');
				$content = $this->input->post('content');
		
				$data = array(
						'name' => $name,
						'price' => $price,
						'discount' => $discount,
						'catalog_id' => $catalog,
						'warranty' => $warranty,
						'gifts' => $gift,
						'site_title' => $site_title,
						'meta_key' => $meta_key,
						'meta_desc' => $meta_desc,
						'content' => $content
				);
		
				$this->load->library('upload_library');
				$path_file = './upload/product';
				$image_link = $this->upload_library->upload_file($path_file, 'image');
				if(!empty($image_link['file_name'])){
					$data['image_link'] = $image_link['file_name'];
				}
		
				$image_list = $this->upload_library->upload_files($path_file, 'image_list');
				foreach($image_list as $image){
					if(!empty($image['file_name'])){		
						$data['image_list'][] = $image['file_name'];
					}
				}
				if(!empty($data['image_list']) && count($data['image_list']) > 0)
					$data['image_list'] = json_encode($data['image_list']);
		
				if($this->product_model->update($product_id, $data)){
					$this->session->set_flashdata('message', 'Cập nhật sản phẩm thành công');
				} else {
					$this->session->set_flashdata('message', 'Chưa cập nhật được sản phẩm');
				}
				redirect(admin_url('product'));
			}
		}
		
		// Danh sach danh muc
		$this->load->model('catalog_model');
		$input = array(
				'where' => array(
						'parent_id' => '0'
				)
		);
		$catalogs = $this->catalog_model->get_list($input);
		foreach ( $catalogs as $c ) {
			$input = array(
					'where' => array(
							'parent_id' => $c->id
					)
			);
			$sub_catalogs = $this->catalog_model->get_list($input);
			$c->sub = $sub_catalogs;
		}
		
		$this->data['catalogs'] = $catalogs;
		$this->data['info'] = $product;
		$this->data['temp'] = 'admin/product/edit';
		$this->load->view($this->main_layout, $this->data);
	}
	
	public function delete(){
		$product_id = intval($this->uri->segment(4));
		$this->__del($product_id, TRUE);
	}
	
	public function delete_all(){
		$ids = $this->input->post('ids');
		$msg = array(
				'deleted' => array(),
				'error' => array()
		);
		foreach ( $ids as $id ) {
			if ($this->__del($id)) {
				$msg['deleted'][] = $id;
			} else {
				$msg['error'][] = $id;
			}
		}
		echo json_encode($msg);
	}
	
	private function __del($product_id, $redirect = FALSE){
		$product = $this->product_model->get_row($product_id);
		if(!$product){
			if($redirect){
				$this->session->set_flashdata('message', 'Không tồn tại sản phẩm này');
				redirect(admin_url('product'));
			} else {
				return FALSE;
			}
		}

		if($this->product_model->delete($product_id)){
			if(!empty($product->image_link)){
				$image_link = './upload/product/'.$product->image_link;
				if(file_exists($image_link)){
					unlink($image_link);
				}
			}
			
			if(!empty($product->image_list)){
				$image_list = json_decode($product->image_list);
				foreach($image_list as $image_link){
					$image_link = './upload/product/'.$image_link;
					if(file_exists($image_link)){
						unlink($image_link);
					}
				}
			}
			
			if($redirect){
				$this->session->set_flashdata('message', 'Xóa thành công sản phẩm');
				redirect(admin_url('product'));
			} else {
				return TRUE;
			}
		}
		return FALSE;
	}
}