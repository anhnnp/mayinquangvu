<?php
class News extends MY_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('news_model');
	}

	public function index()
	{
		$this->load->helper('date_helper');
		$this->load->library('pagination');
		
		// Link phan trang
		$input['select'] = 'id';
		$this->__filter_input($input);
		$total = $this->news_model->get_totals($input);
		$config = array(
				'total_rows' => $total,
				'per_page' => 10,
				'base_url' => admin_url('news'),
				'uri_segment' => 4,
				'reuse_query_string' => TRUE,
				'use_page_numbers' => TRUE,
				'next_link' => 'Trang kế tiếp',
				'prev_link' => 'Trang trước'
		);
		$this->pagination->initialize($config);
		
		$start_index = intval($this->uri->segment(4));
		if (empty($start_index) || $start_index < 0) {
			$start_index = 0;
		} else {
			$start_index = ($start_index-1)*$config['per_page'];
		}
		
		$input = array(
				'limit' => array(
						0 => $config['per_page'],
						1 => $start_index
				)
		);
		$this->__filter_input($input);
		$list = $this->news_model->get_list($input);
		
		$this->data['message'] = $this->session->flashdata('message');
		$this->data['list'] = $list;
		$this->data['total'] = $total;
		$this->data['temp'] = 'admin/news/index';
		$this->load->view($this->main_layout, $this->data);
	}

	private function __filter_input(&$input = array())
	{
		$id_news = intval($this->input->get('id'));
		if (! empty($id_news) && $id_news > 0) {
			$input['where'] = array(
					'id' => $id_news
			);
		}
		
		$title_news = $this->input->get('title');
		if (! empty($title_news)) {
			$input['like'] = array(
					0 => 'title',
					1 => $title_news
			);
		}
	}

	public function add()
	{
		$this->load->library('form_validation');
		$this->load->helper('form');
		$this->load->library('upload_library');
		
		if ($this->input->post()) {
			$this->form_validation->set_rules('title', 'tiêu đề', 'required|max_length[255]');
			$this->form_validation->set_rules('content', 'nội dung', 'required');
			
			if ($this->form_validation->run()) {
				$title = $this->input->post('title');
				$meta_desc = $this->input->post('meta_desc');
				$meta_key = $this->input->post('meta_key');
				$content = $this->input->post('content');
				$intro = $this->input->post('intro');
				
				if (empty($intro)) {
					if ($cont_len = strlen(strip_tags($content)) < 100) {
						$intro = substr(strip_tags($content), 0, 100);
					} else {
						$intro = substr(strip_tags($content), 0, 100);
					}
				}
				
				$data = array(
						'title' => $title,
						'meta_desc' => $meta_desc,
						'meta_key' => $meta_key,
						'content' => $content,
						'created' => time(),
						'intro' => ltrim($intro)
				);
				
				$image_link = $this->upload_library->upload_file('./upload/news', 'image');
				if (! empty($image_link['file_name'])) {
					$data['image_link'] = $image_link['file_name'];
				}
				
				if ($this->news_model->create($data)) {
					$this->session->set_flashdata('message', 'Tạo bài viết mới thành công');
				} else {
					$this->session->set_flashdata('message', 'Chưa tạo được bài viết');
				}
				redirect(admin_url('news'));
			}
		}
		
		$this->data['temp'] = 'admin/news/add';
		$this->load->view($this->main_layout, $this->data);
	}

	public function edit()
	{
		$this->load->library('upload_library');
		$this->load->library('form_validation');
		$this->load->helper('form');
		
		$id_news = intval($this->uri->segment(4));
		$news = $this->news_model->get_row($id_news);
		if (! $news) {
			$this->session->set_flashdata('message', 'Không tồn tại bài viết này');
			redirect(admin_url('news'));
		}
		
		if ($this->input->post()) {
			$this->form_validation->set_rules('title', 'tiêu đề', 'required|max_length[255]');
			$this->form_validation->set_rules('content', 'nội dung', 'required');
			
			if ($this->form_validation->run()) {
				$title = $this->input->post('title');
				$meta_desc = $this->input->post('meta_desc');
				$meta_key = $this->input->post('meta_key');
				$content = $this->input->post('content');
				$intro = $this->input->post('intro');
				if (empty($intro)) {
					if ($cont_len = strlen(strip_tags($content)) < 100) {
						$intro = substr(strip_tags($content), 0, 100);
					} else {
						$intro = substr(strip_tags($content), 0, 100);
					}
				}
				
				$data = array(
						'title' => $title,
						'meta_desc' => $meta_desc,
						'meta_key' => $meta_key,
						'content' => $content,
						'created' => time(),
						'intro' => ltrim($intro)
				);
				
				$image_link = $this->upload_library->upload_file('./upload/news', 'image_link');
				if (! empty($image_link['file_name'])) {
					$data['image_link'] = $image_link['file_name'];
				}
				
				if ($this->news_model->update($id_news, $data)) {
					$this->session->set_flashdata('message', 'Sửa bài viết mới thành công');
				} else {
					$this->session->set_flashdata('message', 'Chưa sửa được bài viết');
				}
				redirect(admin_url('news'));
			}
		}
		
		$this->data['info'] = $news;
		$this->data['temp'] = 'admin/news/edit';
		$this->load->view($this->main_layout, $this->data);
	}

	public function delete()
	{
		$id_news = intval($this->uri->segment(4));
		$this->__del($id_news, TRUE);
	}

	public function delete_all()
	{
		$ids = $this->input->post('ids');
		$msg = array(
				'deleted' => array(),
				'error' => array()
		);
		foreach ( $ids as $id ) {
			if ($this->__del($id)) {
				$msg['deleted'][] = $id;
			} else {
				$msg['error'][] = $id;
			}
		}
		echo json_encode($msg);
	}

	private function __del($id, $redirect = FALSE)
	{
		$news = $this->news_model->get_row($id);
		if (!$news) {
			if ($redirect) {
				$this->session->set_flashdata('message', 'Không tồn tại bài viết này');
				redirect(admin_url('news'));
			}
			return FALSE;
		}
		
		if ($this->news_model->delete($id)) {
			if (! empty($news->image_link)) {
				$file = './upload/news/' . $news->image_link;
				if (file_exists($file)) {
					unlink($file);
				}
			}
			if ($redirect) {
				$this->session->set_flashdata('message', 'Xóa thành công bài viết');
				redirect(admin_url('news'));
			}
			return TRUE;
		} else {
			if ($redirect) {
				$this->session->set_flashdata('message', 'Không xóa được bài viết');
				redirect(admin_url('news'));
			}
			return FALSE;
		}
	}
}