<?php
class Catalog extends MY_Controller {
	function __construct(){
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->helper('form');
		$this->load->model('catalog_model');
	}
	
	public function index(){
		$this->data['message'] = $this->session->flashdata('message');
		$list_catalog = $this->catalog_model->get_list();
		foreach($list_catalog as $catalog){
			$parent = $this->catalog_model->get_parent($catalog->parent_id);
			if($parent)
				$catalog->parent_name = $parent->name;
			else $catalog->parent_name = '---';
		}
		$this->data['list'] = $list_catalog;
		$this->data['total'] = $this->catalog_model->get_totals();		
		$this->data['temp'] = 'admin/catalog/index';
		$this->load->view($this->main_layout, $this->data);
	}
	
	public function add(){	
		if($this->input->post()){
			$this->form_validation->set_rules('name', 'Tên danh mục', 'required|max_length[255]');
			if($this->form_validation->run()){
				$name = $this->input->post('name');
				$parent_id = $this->input->post('parent');
				$sort_order = $this->input->post('sort_order');
				$this->data = array(
						'name' => $name,
						'parent_id' => $parent_id,
						'sort_order' => $sort_order
				);
				if($this->catalog_model->create($this->data)){
					$this->session->set_flashdata('message', 'Tạo mới danh mục thành công');
				} else {
					$this->session->set_flashdata('message', 'Chưa tạo được danh mục mới');
				}
				redirect(admin_url('catalog'));
			}
		}
		
		$input = array('where' => array('parent_id' => '0'));
		$list_parent = $this->catalog_model->get_list($input);
		$this->data['parent'] = $list_parent;
		$this->data['temp'] = 'admin/catalog/add';
		$this->load->view($this->main_layout, $this->data);
	}
	
	public function edit(){
		$catalog = $this->checked_id();
		$id_catalog = $catalog->id;	
		if($this->input->post()){
			$this->form_validation->set_rules('name', 'Tên danh mục', 'required|max_length[255]');
			if($this->form_validation->run()){
				$name = $this->input->post('name');
				$parent_id = $this->input->post('parent');
				$sort_order = $this->input->post('sort_order');
				$this->data = array(
						'name' => $name,
						'parent_id' => $parent_id,
						'sort_order' => $sort_order
				);
				if($this->catalog_model->update($id_catalog, $this->data)){
					$this->session->set_flashdata('message', 'Chỉnh sửa danh mục thành công');
				} else {
					$this->session->set_flashdata('message', 'Chưa sửa được danh mục');
				}
				redirect(admin_url('catalog'));
			}
		}
		
		$input = array('where' => array('parent_id' => '0'));
		$list_parent = $this->catalog_model->get_list($input);
		$this->data['parent'] = $list_parent;
		
		$this->data['info'] = $catalog;
		$this->data['temp'] = 'admin/catalog/edit';
		$this->load->view($this->main_layout, $this->data);
	}
	
	public function delete(){
		$id_catalog = $this->checked_id()->id;
		if($this->catalog_model->delete($id_catalog)){
			$this->session->set_flashdata('message', 'Xóa danh mục thành công');
			redirect(admin_url('catalog'));
		}
	}
	
	private function checked_id(){
		$id_catalog = $this->uri->rsegment(3);
		if(!is_numeric($id_catalog)){
			$this->session->set_flashdata('message', 'Không tồn tại danh mục này');
			redirect(admin_url('catalog'));
		}
		$id_catalog = intval($id_catalog);
		$info = $this->catalog_model->get_row($id_catalog);
		if(!$info){
			$this->session->set_flashdata('message', 'Không tồn tại danh mục này');
			redirect(admin_url('catalog'));
		}
		return $info;
	}
}