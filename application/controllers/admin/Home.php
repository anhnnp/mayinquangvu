<?php

class Home extends MY_Controller {
    public function index(){
    	$this->load->model('transaction_model');
        $this->data['temp'] = 'admin/home/index';
        
        $this->data['list'] = $this->transaction_model->get_list(array(
				'limit' => array(10, 1)));
        
        $this->load->view('admin/main', $this->data);
    }
}
