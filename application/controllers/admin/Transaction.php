<?php
class Transaction extends MY_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('transaction_model');
	}

	function index()
	{
		$this->load->helper('date_helper');
		// Phan trang
		$this->load->library('pagination');
		$input = array(
				'select' => 'id'
		);
		$this->__filter_query($input);
		$totals = $this->transaction_model->get_totals($input);
		$config = array(
				'total_rows' => $totals,
				'base_url' => admin_url('transaction/index'),
				'per_page' => 5,
				'uri_segment' => 4,
				'num_links' => 2,
				'reuse_query_string' => TRUE,
				'use_page_numbers' => TRUE,
				'next_link' => 'Trang kế tiếp',
				'prev_link' => 'Trang trước'
		);
		$this->pagination->initialize($config);
		
		$start_index = $this->uri->segment(4);
		if (! $start_index && ! is_int($start_index)) {
			$start_index = 0;
		} else {
			$start_index = ($start_index-1)*$config['per_page'];
		}
		
		$input = array(
				'limit' => array(
						$config['per_page'],
						$start_index
				)
		);
		
		// Loc du lieu neu co
		$this->__filter_query($input);
		
		$list = $this->transaction_model->get_list($input);
		
		$this->data['message'] = $this->session->flashdata('message');
		$this->data['total'] = $totals;
		$this->data['list'] = $list;
		
		$this->data['temp'] = 'admin/transaction/index';
		$this->load->view($this->main_layout, $this->data);
	}

	private function __filter_query(&$input)
	{
		// Loc theo id
		$id_filter = intval($this->input->get('id'));
		if ($id_filter > 0) {
			$input['where']['id'] = $id_filter;
		}
		// Loc theo phuong thuc thanh toan
		$payment_method = $this->input->get('payment_method');
		if (! empty($payment_method)) {
			$input['where']['payment'] = $payment_method;
		}
		// Loc theo ten nguoi mua hang
		$user = $this->input->get('user');
		if (! empty($user)) {
			$input['like'][] = 'user_name';
			$input['like'][] = $user;
		}
		// Loc theo trang thai giao dich
		$trans_status = $this->input->get('trans_status');
		if (! empty($trans_status)) {
			$trans_status = intval($this->input->get('trans_status'));
			if ($trans_status >= 0) {				
				$input['where']['status'] = $trans_status;
			}
		}
		// Loc theo thoi gian
		$from_date = $this->input->get('from_date');
		$to_date = $this->input->get('to_date');
		if (! empty($from_date)) {
			$from_date = string_to_timestamp('Y-m-d', $from_date);
			$input['where']['created >='] = $from_date;
		}
		if (! empty($to_date)) {
			$to_date = string_to_timestamp('Y-m-d', $to_date);
			$input['where']['created <'] = $to_date;
		}
		// show_text($this->db->last_query(), 1);
		return $input;
	}

	function view()
	{
		$id_trans = intval($this->uri->rsegment(3));
		$transaction = $this->transaction_model->get_row($id_trans);
		if(!$transaction) redirect(admin_url('transaction'));
		
// 		show_text($transaction);
		
		$this->load->model('order_model');
		$list_products_order = $this->order_model->get_list(array('where' => array('transaction_id' => $transaction->id)));
// 		show_text($this->db->last_query());
		
// 		show_text($list_products_order, 1);
		
		$total_product = 0;
		if(!empty($list_products_order)){
			$this->load->model('product_model');			
			foreach($list_products_order as &$p){
				$product = $this->product_model->get_row($p->product_id);
				$p->product_name = $product->name;
				$p->image_link = $product->image_link;
				$p->discount = $product->discount;
				$p->price = $product->price;	
				$total_product += intval($p->qty);
			}
		}
		
		$this->data['message'] = $this->session->flashdata('message');
		$this->data['total'] = $total_product;
		$this->data['list_products'] = $list_products_order;
		$this->data['transaction'] = $transaction;
		$this->data['temp'] = 'admin/transaction/view';
		$this->load->view($this->main_layout, $this->data);
	}
	
	function edit(){
		$id_trans = intval($this->uri->rsegment(3));
		$transaction = $this->transaction_model->get_row($id_trans);
		if(!$transaction) redirect(admin_url('transaction'));
		
		// 		show_text($transaction);
		
		$this->load->model('order_model');
		$list_products_order = $this->order_model->get_list(array('where' => array('transaction_id' => $transaction->id)));
		// 		show_text($this->db->last_query());
		
		// 		show_text($list_products_order, 1);
		
		$total_product = 0;
		if(!empty($list_products_order)){
			$this->load->model('product_model');
			foreach($list_products_order as &$p){
				$product = $this->product_model->get_row($p->product_id);
				$p->product_name = $product->name;
				$p->image_link = $product->image_link;
				$p->discount = $product->discount;
				$p->price = $product->price;
				$total_product += intval($p->qty);
			}
		}
		
		$this->data['message'] = $this->session->flashdata('message');
		$this->data['total'] = $total_product;
		$this->data['list_products'] = $list_products_order;
		$this->data['transaction'] = $transaction;
		$this->data['temp'] = 'admin/transaction/edit';
		$this->load->view($this->main_layout, $this->data);
	}

	function delete()
	{
		
	}
}