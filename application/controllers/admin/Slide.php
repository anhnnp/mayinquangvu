<?php
class Slide extends MY_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('slide_model');
	}

	public function index()
	{
		$total = $this->slide_model->get_totals();
		$list = $this->slide_model->get_list();
		
		$this->data['message'] = $this->session->flashdata('message');
		$this->data['list'] = $list;
		$this->data['total'] = $total;
		$this->data['temp'] = 'admin/slide/index';
		$this->load->view($this->main_layout, $this->data);
	}

	public function add()
	{
		$this->load->library('form_validation');
		$this->load->helper('form');
		$this->load->library('upload_library');
		
		if ($this->input->post()) {
			$this->form_validation->set_rules('name', 'tên slide', 'required|max_length[255]');
			
			if ($this->form_validation->run()) {
				$name = $this->input->post('name');
				$image_name = $this->input->post('image_name');
				$link = $this->input->post('link');
				$sort_order = $this->input->post('sort_order');
				$info = $this->input->post('info');
				
				$data = array(
						'name' => $name,
						'image_name' => $image_name,
						'link' => $link,
						'sort_order' => $sort_order,
						'info' => $info,
				);
				
				$image_link = $this->upload_library->upload_file('./upload/slide', 'image');
				if (! empty($image_link['file_name'])) {
					$data['image_link'] = $image_link['file_name'];
				}
				
				if ($this->slide_model->create($data)) {
					$this->session->set_flashdata('message', 'Tạo slide mới thành công');
				} else {
					$this->session->set_flashdata('message', 'Chưa tạo được slide');
				}
				redirect(admin_url('slide'));
			}
		}
		
		$this->data['temp'] = 'admin/slide/add';
		$this->load->view($this->main_layout, $this->data);
	}

	public function edit()
	{
		$this->load->library('upload_library');
		$this->load->library('form_validation');
		$this->load->helper('form');
		
		$id_slide = intval($this->uri->segment(4));
		$slide = $this->slide_model->get_row($id_slide);
		if (! $slide) {
			$this->session->set_flashdata('message', 'Không tồn tại slide này');
			redirect(admin_url('slide'));
		}
		
		if ($this->input->post()) {
			$this->form_validation->set_rules('name', 'tên slide', 'required|max_length[255]');
			
			if ($this->form_validation->run()) {
				$name = $this->input->post('name');
				$image_name = $this->input->post('image_name');
				$link = $this->input->post('link');
				$sort_order = $this->input->post('sort_order');
				$info = $this->input->post('info');
				
				$data = array(
						'name' => $name,
						'image_name' => $image_name,
						'link' => $link,
						'sort_order' => $sort_order,
						'info' => $info,
				);
				
				$image_link = $this->upload_library->upload_file('./upload/slide', 'image_link');
				if (! empty($image_link['file_name'])) {
					$data['image_link'] = $image_link['file_name'];
				}
				
				if ($this->slide_model->update($id_slide, $data)) {
					$this->session->set_flashdata('message', 'Sửa slide thành công');
				} else {
					$this->session->set_flashdata('message', 'Chưa sửa được slide');
				}
				redirect(admin_url('slide'));
			}
		}
		
		$this->data['info'] = $slide;
		$this->data['temp'] = 'admin/slide/edit';
		$this->load->view($this->main_layout, $this->data);
	}

	public function delete()
	{
		$id_slide = intval($this->uri->segment(4));
		$this->__del($id_slide, TRUE);
	}

	public function delete_all()
	{
		$ids = $this->input->post('ids');
		$msg = array(
				'deleted' => array(),
				'error' => array()
		);
		foreach ( $ids as $id ) {
			if ($this->__del($id)) {
				$msg['deleted'][] = $id;
			} else {
				$msg['error'][] = $id;
			}
		}
		echo json_encode($msg);
	}

	private function __del($id, $redirect = FALSE)
	{
		$slide = $this->slide_model->get_row($id);
		if (!$slide) {
			if ($redirect) {
				$this->session->set_flashdata('message', 'Không tồn tại slide này');
				redirect(admin_url('slide'));
			}
			return FALSE;
		}
		
		if ($this->slide_model->delete($id)) {
			if (! empty($slide->image_link)) {
				$file = './upload/slide/' . $slide->image_link;
				if (file_exists($file)) {
					unlink($file);
				}
			}
			if ($redirect) {
				$this->session->set_flashdata('message', 'Xóa thành công slide');
				redirect(admin_url('slide'));
			}
			return TRUE;
		} else {
			if ($redirect) {
				$this->session->set_flashdata('message', 'Không xóa được slide');
				redirect(admin_url('slide'));
			}
			return FALSE;
		}
	}
}