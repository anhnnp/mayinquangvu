<?php 
class Order extends MY_Controller {
	function __construct() {
		parent::__construct();
		$this->load->model('order_model');
	}
	
	function index() {
		$this->load->helper('date_helper');
		$this->load->library('pagination');
		$input = array(
				'select' => 'id'
		);
		$this->__filter_query($input);
		$totals = $this->order_model->get_totals($input);
		$config = array(
				'total_rows' => $totals,
				'base_url' => admin_url('order/index'),
				'per_page' => 5,
				'uri_segment' => 4,
				'num_links' => 2,
				'reuse_query_string' => TRUE,
				'use_page_numbers' => TRUE,
				'next_link' => 'Trang kế tiếp',
				'prev_link' => 'Trang trước'
		);
		$this->pagination->initialize($config);
		
		$start_index = $this->uri->segment(4);
		if (! $start_index && ! is_int($start_index)) {
			$start_index = 0;
		} else {
			$start_index = ($start_index-1)*$config['per_page'];
		}
		
		$input = array(
				'limit' => array(
						$config['per_page'],
						$start_index
				)
		);
		
		// Loc du lieu neu co
		$this->__filter_query($input);
		
		$list_products_order = $this->order_model->get_list($input);
		
		$this->data['message'] = $this->session->flashdata('message');
		$this->data['total'] = $totals;
		$this->data['list'] = $list_products_order;
		$this->data['temp'] = 'admin/order/index';
		$this->load->view($this->main_layout, $this->data);
	}
	
	private function __filter_query(&$input)
	{
		// Loc theo id
		$id_filter = intval($this->input->get('id'));
		if ($id_filter > 0) {
			$input['where']['order.id'] = $id_filter;
		}
		// Loc theo trang thai gui hang san pham
		$product_status = $this->input->get('product_status');
		if (! empty($product_status)) {
			$input['where']['order.status'] = $product_status;
		}
		// Loc theo ten nguoi mua hang
		$user = $this->input->get('user');
		if (! empty($user)) {
			$input['like'][] = 'transaction.user_name';
			$input['like'][] = $user;
		}
		// Loc theo trang thai giao dich
		$trans_status = $this->input->get('trans_status');
		if (! empty($trans_status)) {
			$trans_status = intval($this->input->get('trans_status'));
			if ($trans_status >= 0) {
				$input['where']['transaction.status'] = $trans_status;
			}
		}
		// Loc theo thoi gian
		$from_date = $this->input->get('from_date');
		$to_date = $this->input->get('to_date');
		if (! empty($from_date)) {
			$from_date = string_to_timestamp('Y-m-d', $from_date);
			$input['where']['transaction.created >='] = $from_date;
		}
		if (! empty($to_date)) {
			$to_date = string_to_timestamp('Y-m-d', $to_date);
			$input['where']['transaction.created <'] = $to_date;
		}
		
		$input['select'] = 'order.amount as order_amount, transaction.amount as trans_amount, order.status as order_status, transaction.status as trans_status, order.*,
				product.name as product_name, product.discount, product.image_link, product.price, transaction.created as created';
		
		$input['join'] = array(
				'transaction' => 'transaction.id = order.transaction_id',
				'product' => 'product.id = order.product_id'
		);
				
		return $input;
	}
}