<?php
class User extends MY_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('user_model');
	}

	function index()
	{
		$user = $this->get_user();
		if (! $user)
			redirect(base_url('user/login'));
		
		$this->data['info'] = $user;
		$this->data['temp'] = 'site/user/index';
		$this->load->view('site/layout', $this->data);
	}

	function register()
	{
		if($this->session->userdata('user_login')){
			redirect(base_url('user'));
		}
		
		$this->load->library('form_validation');
		$this->load->helper('form');
		
		if ($this->input->post()) {
			$this->form_validation->set_rules('name', 'tên thành viên', 'required|max_length[255]');
			$this->form_validation->set_rules('email', 'địa chỉ email', 'required|max_length[255]|valid_email|is_unique[user.email]');
			$this->form_validation->set_rules('phone', 'số điện thoại', 'required|max_length[15]|min_length[10]|is_natural');
			$this->form_validation->set_rules('address', 'địa chỉ', 'required');
			$this->form_validation->set_rules('password', 'mật khẩu', 'required|max_length[255]');
			$this->form_validation->set_rules('re_password', 'nhập lại mật khẩu', 'required|max_length[255]|matches[password]');

			if ($this->form_validation->run()) {
				
				$name = $this->input->post('name');
				$email = $this->input->post('email');
				$phone = $this->input->post('phone');
				$address = $this->input->post('address');
				$password = $this->input->post('password');
				$re_password = $this->input->post('re_password');
				
				$data = array(
						'name' => $name,
						'email' => $email,
						'address' => $address,
						'phone' => $phone,
						'password' => md5($password),
						'created' => time()
				);
											
				if ($this->user_model->create($data)) {
					$this->session->set_flashdata('message', 'Đăng ký tài khoản thành viên thành công');
					redirect();
				} else {
					$this->session->set_flashdata('message', 'Đăng ký tài khoản thành viên có lỗi');
				}
			}			
		}
		
		$this->data['temp'] = 'site/user/register';
		$this->load->view('site/layout', $this->data);
	}

	function edit()
	{
		$user = $this->get_user();
		if(!$user) redirect(base_url('user/register'));
		
		if ($this->input->post()) {
			$this->load->library('form_validation');
			$this->load->helper('form');
			
			$this->form_validation->set_rules('name', 'tên thành viên', 'required|max_length[255]');
			$this->form_validation->set_rules('email', 'địa chỉ email', 'required|max_length[255]|valid_email|is_unique[user.email]');
			$this->form_validation->set_rules('phone', 'số điện thoại', 'required|max_length[15]|min_length[10]|is_natural');
			$this->form_validation->set_rules('address', 'địa chỉ', 'required');
			
			$password = $this->input->post('password');
			$re_password = $this->input->post('re-password');
			if (! empty($password) || ! empty($re_password)) {
				$this->form_validation->set_rules('password', 'mật khẩu', 'required|max_length[255]');
				$this->form_validation->set_rules('re_password', 'nhập lại mật khẩu', 'required|max_length[255]|matches[password]');
			}
			
			if ($this->form_validation->run()) {
				$name = $this->input->post('name');
				$email = $this->input->post('email');
				$phone = $this->input->post('phone');
				$address = $this->input->post('address');
				
				$data = array(
						'name' => $name,
						'email' => $email,
						'address' => $address,
						'phone' => $phone
				);
				
				if (! empty($password)) {
					$data['password'] = md5($password);
				}
				
				if ($this->user_model->create($data)) {
					$this->session->set_flashdata('message', 'Đăng ký tài khoản thành viên thành công');
					redirect();
				} else {
					$this->session->set_flashdata('message', 'Đăng ký tài khoản thành viên có lỗi');
				}
			}
			
			$this->data['info'] = $user;
			$this->data['temp'] = 'site/user/register';
			$this->load->view('site/layout', $this->data);
		}
	}

	function login()
	{
		if($this->session->userdata('user_login')){
			redirect(base_url('user'));
		}
		
		$this->load->library('form_validation');
		$this->load->helper('form');
		
		if ($this->input->post()) {			
			$this->form_validation->set_rules('email', 'địa chỉ email', 'required|max_length[255]|callback_check_login');
			$this->form_validation->set_rules('password', 'mật khẩu', 'required|max_length[255]');
			
			if ($this->form_validation->run()) {
				$this->session->set_userdata('user_login', array('id' => $this->get_user()->id, 'name' => $this->get_user()->name));
				
				redirect(base_url());
			}
		}
		
		$this->data['temp'] = 'site/user/login';
		$this->load->view('site/layout', $this->data);
	}
	
	function logout(){
		if($this->session->userdata('user_login')){
			$this->session->unset_userdata('user_login');
			redirect();
		}
	}

	function check_login()
	{
		$user = $this->get_user();
		if ($user)
			return TRUE;
		$this->form_validation->set_message(__FUNCTION__, 'Tài khoản không hợp lệ');
		return FALSE;
	}

	private function get_user()
	{
		$id_user = intval($this->session->userdata('user_login')['id']);
		$user = NULL;
		if (!empty($id_user)) {
			$user = $this->user_model->get_row($id_user);
		} else {
			if ($this->input->post()) {
				$email = $this->input->post('email');
				$pass = $this->input->post('password');
				
				$input = array(
						'email' => $email,
						'password' => md5($pass)
				);
				
				$user = $this->user_model->get_row_rule($input);
			}
		}
		return $user;
	}
}