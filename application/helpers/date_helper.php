<?php

include_once './system/helpers/date_helper.php';

function get_date($time, $datetime = FALSE){
	$format_date = 'd-m-Y';
	if($datetime) $format_date .= ' H:i:m';
	return date($format_date, $time);
}

function get_time_info($datetime){
	$info = array();
	$info['d'] = intval(date('d', $datetime));
	$info['m'] = intval(date('m', $datetime));
	$info['y'] = intval(date('Y', $datetime));
	$info['h'] = intval(date('H', $datetime));
	$info['i'] = intval(date('i', $datetime));
	$info['s'] = intval(date('s', $datetime));
	return $info;
}

function string_to_timestamp($format, $datetime){
	$dtime = DateTime::createFromFormat($format, $datetime);
	if($dtime){
		return $dtime->format('U');
	}
	return FALSE;
}

function get_time_form_date(){
	
}

function get_time_between(){
	
}

function get_time_between_day(){
	
}

