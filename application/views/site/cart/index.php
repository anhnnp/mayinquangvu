
<div class="box-center">
	<?php if($total_items > 0): ?>
	<!-- The box-center cart-->
	<div class="tittle-box-center">
		<h2>Giỏ hàng có <?php echo $total_items; ?> sản phẩm</h2>
	</div>
	<div class="box-content-center cart">
		<style>
			#form_cart table, tr, td {
 				border: 1px solid black;
				border-collapse: collapse;
				padding: 5px;
			}
		</style>
		<form action="<?php echo base_url('cart/update'); ?>" method="post" id="form_cart">
			<table width="100%">
				<thead>
					<tr>
						<td>Sản phẩm</td>
						<td>Giá bán</td>
						<td>Số lượng</td>
						<td>Tổng giá</td>
						<td>Xóa</td>
					</tr>
				</thead>
				<tbody>
					<?php $total_price = 0; ?>
					<?php foreach ($cart as $row): ?>
					<?php $total_price += $row['subtotal']; ?>
					<tr>
						<td><?php echo $row['name']; ?></td>
						<td><?php echo number_format($row['price']); ?></td>
						<td><input type="text" name="qty_<?php echo $row['id']; ?>" value="<?php echo $row['qty']; ?>"></td>
						<td><?php echo number_format($row['subtotal']); ?></td>
						<td><a href="<?php echo base_url('cart/delete/'.$row['id']); ?>" title="">Xóa</a></td>
					</tr>
					<?php endforeach; ?>
				</tbody>
				<tfoot>
					<tr>
						<td colspan="5">Tổng số tiền thanh toán: <span style="color:red;"><?php echo $total_price; ?></span>&nbsp; <a href="<?php echo base_url('cart/delete'); ?>">Xóa toàn bộ</a></td>
					</tr>
					<tr>
						<td colspan="3"><input class="button" type="submit" name="btn_update_cart" value="Cập nhật"></td>
						<td colspan="2"><a href="<?php echo base_url('order/checkout'); ?>" title="checkout" class="button">Checkout</a></td>
					</tr>
				</tfoot>
			</table>
		</form>
	</div>
	<?php else: ?>
	<h4>Không có sản phẩm nào trong giỏ hàng</h4>
	<?php endif; ?>
</div>


