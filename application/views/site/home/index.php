<?php $this->load->view('site/slide', $this->data); ?>

<!-- lay san pham moi nhat -->
<div class="box-center">
	<!-- The box-center product-->
	<div class="tittle-box-center">
		<h2>Sản phẩm mới</h2>
	</div>
	<div class="box-content-center product">
		<!-- The box-content-center -->
		<?php foreach ($products_last as $product): ?>
		<div class='product_item'>
			<h3>
				<a href="<?php echo base_url('product/view/'.$product->id); ?>" title="<?php echo $product->name; ?>"><?php echo $product->name; ?></a>
			</h3>
			<div class='product_img'>
				<a href="<?php echo base_url('product/view/'.$product->id); ?>" title="<?php echo $product->name; ?>"> <img src="<?php echo upload_url('product/'.$product->image_link); ?>" alt="<?php echo $product->image_link; ?>" />
				</a>
			</div>
			<p class='price'>
				<?php 
				if(!empty($product->discount) && $product->discount > 0): ?>
					<?php echo number_format($product->price-$product->discount).' đ'; ?>
					<span class='price_old'><?php echo number_format($product->price); ?> đ</span>
				<?php else: ?>
					<?php echo number_format($product->price) .' đ'; ?>	
				<?php endif; ?>							
			</p>
			<div class='raty' style='margin: 10px auto;' id='9' data-score='4'></div>
			<div class='action'>
				<p style='float: left; margin-left: 10px'>
					Lượt xem: <b><?php echo $product->view; ?></b>
				</p>
				<a class='button' href="them-vao-gio-9.html" title='Mua ngay'>Mua ngay</a>
				<div class='clear'></div>
			</div>
		</div>
		<?php endforeach; ?>
		<div class='clear'></div>
	</div>
	<!-- End box-content-center -->
</div>
<!-- End box-center product-->
<!-- lay san pham dang giam gia -->
<div class="box-center">
	<!-- The box-center product-->
	<div class="tittle-box-center">
		<h2>Sản phẩm giảm giá</h2>
	</div>
	<div class="box-content-center product">
		<!-- The box-content-center -->
		<?php foreach ($products_discount as $product): ?>
		<div class='product_item'>
			<h3>
				<a href="<?php echo base_url('product/view/'.$product->id); ?>" title="<?php echo $product->name; ?>"><?php echo $product->name; ?></a>
			</h3>
			<div class='product_img'>
				<a href="<?php echo base_url('product/view/'.$product->id); ?>" title="<?php echo $product->name; ?>"> <img src="<?php echo upload_url('product/'.$product->image_link); ?>" alt="<?php echo $product->image_link; ?>" />
				</a>
			</div>
			
			<p class='price'>
				<?php 
				if(!empty($product->discount) && $product->discount > 0): ?>
					<?php echo number_format($product->price-$product->discount).' đ'; ?>
					<span class='price_old'><?php echo number_format($product->price); ?> đ</span>
				<?php else: ?>
					<?php echo number_format($product->price) .' đ'; ?>	
				<?php endif; ?>							
			</p>

			<div class='raty' style='margin: 10px auto;' id='8' data-score='3.4'></div>

			<div class='action'>
				<p style='float: left; margin-left: 10px'>
					Lượt xem: <b><?php echo $product->view; ?></b>
				</p>
				<a class='button' href="them-vao-gio-8.html" title='Mua ngay'>Mua ngay</a>
				<div class='clear'></div>
			</div>
		</div>
		<?php endforeach; ?>
		<div class='clear'></div>
	</div>
	<!-- End box-content-center -->
</div>
<!-- End box-center product-->
<!-- lay san pham xem nhieu -->
<div class="box-center">
	<!-- The box-center product-->
	<div class="tittle-box-center">
		<h2>Sản phẩm xem nhiều</h2>
	</div>
	<div class="box-content-center product">
		<!-- The box-content-center -->
		<?php foreach ($count_view as $product): ?>
		<div class='product_item'>
			<h3>
				<a href="<?php echo base_url('product/view/'.$product->id); ?>" title="<?php echo $product->name; ?>"><?php echo $product->name; ?></a>
			</h3>
			<div class='product_img'>
				<a href="<?php echo base_url('product/view/'.$product->id); ?>" title="<?php echo $product->name; ?>"> <img src="<?php echo upload_url('/product/'.$product->image_link); ?>" alt="<?php echo $product->image_link; ?>" />
				</a>
			</div>
			
			<p class='price'>
				<?php 
				if(!empty($product->discount) && $product->discount > 0): ?>
					<?php echo number_format($product->price-$product->discount).' đ'; ?>
					<span class='price_old'><?php echo number_format($product->price); ?> đ</span>
				<?php else: ?>
					<?php echo number_format($product->price) .' đ'; ?>	
				<?php endif; ?>							
			</p>

			<div class='raty' style='margin: 10px auto;' id='8' data-score='3.4'></div>

			<div class='action'>
				<p style='float: left; margin-left: 10px'>
					Lượt xem: <b><?php echo $product->view; ?></b>
				</p>
				<a class='button' href="them-vao-gio-8.html" title='Mua ngay'>Mua ngay</a>
				<div class='clear'></div>
			</div>
		</div>		
		<?php endforeach; ?>
		<div class='clear'></div>
	</div>
	<!-- End box-content-center -->
</div>
<!-- End box-center product-->