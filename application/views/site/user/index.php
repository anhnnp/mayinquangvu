<div class="box-center">
	<!-- The box-center register-->
	<div class="tittle-box-center">
		<h2>Thông tin thành viên</h2>
	</div>
	<div class="box-content-center info">
		<!-- The box-content-center -->
		<h1>Thông tin thành viên</h1>
		<div class="form-row">
			<label class="form-label" for="param_name">Họ và tên:<span class="req">*</span></label>
			<div class="form-item">
				<span><?php echo $info->name; ?></span>
				<div class="clear"></div>
			</div>
			<div class="clear"></div>
		</div>
		<div class="form-row">
			<label class="form-label" for="param_email">Email:<span class="req">*</span></label>
			<div class="form-item">
				<span><?php echo $info->email; ?></span>
				<div class="clear"></div>
			</div>
			<div class="clear"></div>
		</div>

		<div class="form-row">
			<label class="form-label" for="param_password">Số điện thoại:<span class="req">*</span></label>
			<div class="form-item">
				<span><?php echo $info->phone; ?></span>
				<div class="clear"></div>
			</div>
			<div class="clear"></div>
		</div>

		<div class="form-row">
			<label class="form-label" for="param_re_password">Địa chỉ:<span class="req">*</span></label>
			<div class="form-item">
				<textarea><?php echo $info->address; ?></textarea>
				<div class="clear"></div>
			</div>
			<div class="clear"></div>
		</div>
	</div>
	<!-- End box-content-center register-->
</div>
<!-- End box-center -->