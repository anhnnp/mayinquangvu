<div class="box-center">
	<!-- The box-center register-->
	<div class="tittle-box-center">
		<h2>Thông tin đơn hàng</h2>
	</div>
	<div class="box-content-center register">
		<!-- The box-content-center -->
		<h1>Thông tin đơn hàng</h1>
		<form enctype="multipart/form-data" action="<?php echo base_url('order/checkout'); ?>" method="post" class="t-form form_action">
			
			<div class="form-row">
				<label class="form-label" for="param_email">Email:<span class="req">*</span></label>
				<div class="form-item">
					<input type="text" value="<?php echo ($user)?$user->email:set_value('email'); ?>" name="email" id="email" class="input">
					<div class="clear"></div>
					<div id="email_error" class="error"><?php echo form_error('email'); ?></div>
				</div>
				<div class="clear"></div>
			</div>

			<div class="form-row">
				<label class="form-label" for="param_name">Họ và tên:<span class="req">*</span></label>
				<div class="form-item">
					<input type="text" value="<?php echo ($user)?$user->name:set_value('name'); ?>" name="name" id="name" class="input">
					<div class="clear"></div>
					<div id="name_error" class="error"><?php echo form_error('name'); ?></div>
				</div>
				<div class="clear"></div>
			</div>
			
			<div class="form-row">
				<label class="form-label" for="param_phone">Số điện thoại:<span class="req">*</span></label>
				<div class="form-item">
					<input type="text" value="<?php echo ($user)?$user->phone:set_value('phone'); ?>" name="phone" id="phone" class="input">
					<div class="clear"></div>
					<div id="phone_error" class="error"><?php echo form_error('phone'); ?></div>
				</div>
				<div class="clear"></div>
			</div>

			<div class="form-row">
				<label class="form-label" for="param_message">Ghi chú:<span class="req">*</span></label>
				<div class="form-item">
					<textarea name="message" id="message" class="input"><?php echo set_value('message');?></textarea>
					<p>(Nhập địa chỉ và thời gian nhận hàng)</p>
					<div class="clear"></div>
					<div id="message_error" class="error"><?php echo form_error('message');?></div>
				</div>
				<div class="clear"></div>
			</div>
			
			<div class="form-row">
				<label class="form-label" for="param_payment">Phương thức thanh toán:<span class="req">*</span></label>
				<div class="form-item">
					<select name="payment">
						<option value="">---Chọn phương thức thanh toán---</option>
						<option value="nganluong">Ngân lượng</option>
						<option value="offline">Thanh toán khi nhận hàng</option>
					</select>
					<div class="clear"></div>
					<div id="payment_error" class="error"><?php echo form_error('payment');?></div>
				</div>
				<div class="clear"></div>
			</div>

			<div class="form-row">
				<label class="form-label">&nbsp;</label>
				<div class="form-item">
					<input type="submit" name="submit" value="Gửi đơn hàng" class="button">
				</div>
			</div>
		</form>
	</div>
	<!-- End box-content-center register-->
</div>
<!-- End box-center -->