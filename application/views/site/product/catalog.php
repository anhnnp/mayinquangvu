<div class="box-center">
	<!-- The box-center product-->
	<div class="tittle-box-center">
		<h2><?php echo $catalog->name; ?> (Có <?php echo $total;?> sản phẩm)</h2>
	</div>
	<div class="box-content-center product">
		<!-- The box-content-center -->
		<?php foreach ($products as $row): ?>
		<div class='product_item'>
		
			<h3>
				<a href="<?php echo base_url('product/view/'.$row->id); ?>" title="<?php echo $row->name; ?>"><?php echo $row->name; ?></a>
			</h3>
			
			<div class='product_img'>
				<a href="<?php echo base_url('product/view/'.$row->id); ?>" title="<?php echo $row->name; ?>"> <img src="<?php echo upload_url('/product/'.$row->image_link); ?>" alt="<?php echo $row->image_link; ?>" />
				</a>
			</div>
			
			<p class='price'>
				<?php 
				if(!empty($row->discount) && $row->discount > 0): ?>
					<?php echo number_format($row->price-$row->discount).' đ'; ?>
					<span class='price_old'><?php echo number_format($row->price); ?> đ</span>
				<?php else: ?>
					<?php echo number_format($row->price) .' đ'; ?>	
				<?php endif; ?>							
			</p>
			
			<div class='raty' style='margin: 10px auto;' id='8' data-score='3.4'></div>
			
			<div class='action'>
				<p style='float: left; margin-left: 10px'>
					Lượt xem: <b><?php echo $row->view; ?></b>
				</p>
				<a class='button' href="them-vao-gio-8.html" title='Mua ngay'>Mua ngay</a>
				<div class='clear'></div>
			</div>
			
		</div>		
		<?php endforeach; ?>
		<div class='clear'></div>
	</div>
	<!-- End box-content-center -->
</div>
<div class='pagination'><?php echo $this->pagination->create_links(); ?></div>
