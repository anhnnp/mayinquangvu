<div class="box-left">
	<div class="title tittle-box-left">
		<h2>Tìm kiếm theo giá</h2>
	</div>
	<div class="content-box">
		<!-- The content-box -->
		<form class="t-form form_action" method="get" style='padding: 10px' action="<?php echo base_url('product/search/price'); ?>" name="search">

			<div class="form-row">
				<label for="param_price_from" class="form-label" style='width: 70px'>Giá từ:<span class="req">*</span></label>
				<div class="form-item" style='width: 90px'>
					<select class="input" id="price_from" name="price_from">
						<?php for($i = 100; $i < 1000; $i = $i + 100): ?>
						<option value="<?php echo $i*1000; ?>" <?php echo (($i*1000)==$this->input->get('price_from'))?'selected':''; ?>><?php echo number_format($i*1000); ?> đ</option>
						<?php endfor; ?>
						<?php for($i = 1000; $i < 50000; $i = $i + 1000): ?>
						<?php if($i%5000 == 0): ?>
						<option value="<?php echo $i*1000; ?>" <?php echo (($i*1000)==$this->input->get('price_from'))?'selected':''; ?>><?php echo number_format($i*1000); ?> đ</option>
						<?php endif; ?>
						<?php endfor; ?>
					</select>
					<div class="clear"></div>
					<div class="error" id="price_from_error"></div>
				</div>
				<div class="clear"></div>
			</div>
			<div class="form-row">
				<label for="param_price_from" class="form-label" style='width: 70px'>Giá tới:<span class="req">*</span></label>
				<div class="form-item" style='width: 90px'>
					<select class="input" id="price_to" name="price_to">
						<?php for($i = 100; $i < 1000; $i = $i + 100): ?>
						<option value="<?php echo $i*1000; ?>" <?php echo (($i*1000)==$this->input->get('price_to'))?'selected':''; ?>><?php echo number_format($i*1000); ?> đ</option>
						<?php endfor; ?>
						<?php for($i = 1000; $i < 50000; $i = $i + 1000): ?>
						<?php if($i%5000 == 0): ?>
						<option value="<?php echo $i*1000; ?>" <?php echo (($i*1000)==$this->input->get('price_to'))?'selected':''; ?>><?php echo number_format($i*1000); ?> đ</option>
						<?php endif; ?>
						<?php endfor; ?>
					</select>
					<div class="clear"></div>
					<div class="error" id="price_from_error"></div>
				</div>
				<div class="clear"></div>
			</div>

			<div class="form-row">
				<label class="form-label">&nbsp;</label>
				<div class="form-item">
					<input type="submit" class="button" name='' value="Tìm kiềm" style='height: 30px !important; line-height: 30px !important; padding: 0px 10px !important'>
				</div>
				<div class="clear"></div>
			</div>
		</form>
	</div>
	<!-- End content-box -->
</div>


<div class="box-left">
	<div class="title tittle-box-left">
		<h2>Danh mục sản phẩm</h2>
	</div>
	<div class="content-box">
		<!-- The content-box -->
		<ul class="catalog-main">
			<?php foreach ($catalogs as $cat): ?>
			<li><span><a href="<?php echo base_url('product/catalog/'.$cat->id); ?>" title="<?php echo $cat->name; ?>"><?php echo $cat->name; ?></a></span> <!-- lay danh sach danh muc con -->
				<?php if(!empty($cat->subs) && count($cat->subs) > 0): ?>
				<ul class="catalog-sub">
					<?php foreach($cat->subs as $sub): ?>
					<li><a href="<?php echo base_url('product/catalog/'.$sub->id); ?>" title="<?php echo $sub->name; ?>"><?php echo $sub->name; ?></a></li>
					<?php endforeach; ?>
				</ul>
				<?php endif; ?>
			</li>
			<?php endforeach; ?>
		</ul>
	</div>
	<!-- End content-box -->
</div>