<?php $this->load->view('admin/admin/header'); ?>
<div class="line"></div>
<div class="wrapper">
	<form class="form" id="form" action="<?php echo admin_url('admin/edit/'.$user_info->id); ?>" method="post">
		<fieldset>
			<div class="widget">
				<div class="title">
					<img src="<?php echo public_url(); ?>/admin/images/icons/dark/add.png" class="titleIcon">
					<h6>Sửa thành viên</h6>
				</div>

				<div class="formRow">
					<label class="formLeft" for="param_name">Tên biệt danh:<span class="req">*</span></label>
					<div class="formRight">
						<span class="oneTwo"><input name="name" id="param_name" type="text" value="<?php echo $user_info->name; ?>"></span>
						<div class="clear error"><?php echo form_error('name'); ?></div>
					</div>
					<div class="clear"></div>
				</div>

				<div class="formRow">
					<label class="formLeft" for="param_username">Tên tài khoản:<span class="req">*</span></label>
					<div class="formRight">
						<span class="oneTwo"><input name="username" id="param_username" type="text" value="<?php echo $user_info->username; ?>"></span>
						<div class="clear error"><?php echo form_error('username'); ?></div>
					</div>
					<div class="clear"></div>
				</div>

				<div class="formRow">
					<label class="formLeft" for="param_password">Mật khẩu:<span class="req">*</span></label>
					<div class="formRight">
						<span class="oneTwo"><input name="password" id="param_password" type="password" value=""></span>						
						<div class="clear error"><?php echo form_error('password'); ?></div>
						<p>Nếu cập nhật mật khẩu thì mới nhập giá trị</p>
					</div>
					<div class="clear"></div>
				</div>

				<div class="formRow">
					<label class="formLeft" for="param_repassword">Nhập lại mật khẩu:<span class="req">*</span></label>
					<div class="formRight">
						<span class="oneTwo"><input name="repassword" id="param_repassword" type="password" value=""></span>
						<div class="clear error"><?php echo form_error('repassword'); ?></div>
					</div>
					<div class="clear"></div>
				</div>

				<div class="formSubmit">
					<input type="submit" value="Cập nhật" class="redB">
					<input type="reset" value="Hủy bỏ" class="basic">
				</div>
				<div class="clear"></div>
			</div>
		</fieldset>
	</form>
</div>