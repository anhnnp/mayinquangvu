<?php $this->load->view('admin/catalog/header'); ?>
<div class="line"></div>
<div class="wrapper">
	<form class="form" id="form" action="<?php echo admin_url('catalog/add'); ?>" method="post">
		<fieldset>
			<div class="widget">
				<div class="title">
					<img src="<?php echo public_url(); ?>/admin/images/icons/dark/add.png" class="titleIcon">
					<h6>Thêm mới danh mục</h6>
				</div>

				<div class="formRow">
					<label class="formLeft" for="param_name">Tên danh mục:<span class="req">*</span></label>
					<div class="formRight">
						<span class="oneTwo"><input name="name" id="param_name" type="text" value="<?php echo set_value('name'); ?>"></span>
						<div class="clear error"><?php echo form_error('name'); ?></div>
					</div>
					<div class="clear"></div>
				</div>

				<div class="formRow">
					<label class="formLeft" for="param_parent">Danh mục cha:</label>
					<div class="formRight">
						<span class="oneTwo">
							<select name="parent" id="param_parent">
								<option value="0">Danh mục cha</option>
								<?php foreach($parent as $row): ?>
									<option value="<?php echo $row->id; ?>"><?php echo $row->name; ?></option>
								<?php endforeach; ?>
							</select>
						</span>
						<div class="clear error"><?php echo form_error('parent'); ?></div>
					</div>
					<div class="clear"></div>
				</div>

				<div class="formRow">
					<label class="formLeft" for="param_sort_order">Thứ tự hiển thị:</label>
					<div class="formRight">
						<span class="oneTwo"><input name="sort_order" id="param_sort_order" type="text" value="<?php echo set_value('sort_order'); ?>"></span>
						<div class="clear error"><?php echo form_error('sort_order'); ?></div>
					</div>
					<div class="clear"></div>
				</div>

				<div class="formSubmit">
					<input type="submit" value="Thêm mới" class="redB"> <input type="reset" value="Hủy bỏ" class="basic">
				</div>
				<div class="clear"></div>
			</div>
		</fieldset>
	</form>
</div>