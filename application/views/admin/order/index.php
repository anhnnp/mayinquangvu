<?php $this->load->view('admin/order/header'); ?>
<div class="line"></div>

<div class="wrapper" id="main_order">
	<?php $this->load->view('admin/message', $message); ?>
	<div class="widget">
		<div class="title">
			<span class="titleIcon"><input type="checkbox" id="titleCheck" name="titleCheck" /></span>
			<h6>Danh sách giao dịch</h6>
			<div class="num f12">
				Số lượng: <b><?php echo $total; ?></b>
			</div>
		</div>


		<table cellpadding="0" cellspacing="0" width="100%" class="sTable mTable myTable" id="checkAll">

			<thead class="filter">
				<tr>
					<td colspan="9">
						<form class="list_filter form" action="<?php echo admin_url('order'); ?>" method="get">
							<table>
								<tbody>

									<tr>
										<td class="label"><label for="filter_id">Mã đơn hàng</label></td>
										<td class="item"><input name="id" value="<?php echo $this->input->get('id'); ?>" id="filter_id" type="text" /></td>

										<td class="label"><label for="filter_product_status">Trạng thái</label></td>
										<td class="item"><select name="product_status" id="filter_product_status">
												<option value=""></option>
												<option value="0" <?php echo $this->input->get('product_status')=='0'?'selected':'';?>>Chờ xử lý</option>
												<option value="1" <?php echo $this->input->get('product_status')=='1'?'selected':'';?>>Đã gửi hàng</option>
												<option value="2" <?php echo $this->input->get('product_status')=='2'?'selected':'';?>>Hủy</option>
										</select></td>

										<td class="label"><label for="filter_from_date">Từ ngày</label></td>
										<td class="item"><input name="from_date" value="<?php echo $this->input->get('from_date'); ?>" id="filter_form_date" type="date" /></td>

										<td><input type="reset" class="basic" value="Reset" onclick="window.location.href = '<?php echo admin_url('order'); ?>'; "></td>

									</tr>

									<tr>
										<td class="label"><label for="filter_user">Tên khách hàng</label></td>
										<td class="item"><input name="user" value="<?php echo $this->input->get('user'); ?>" id="filter_user" type="text" /></td>

										<td class="label"><label for="filter_trans_status">Thanh toán</label></td>
										<td class="item"><select name="trans_status" id="filter_trans_status">
												<option value=""></option>
												<option value="0" <?php echo $this->input->get('trans_status')=='0'?'selected':'';?>>Chưa thanh toán</option>
												<option value="1" <?php echo $this->input->get('trans_status')=='1'?'selected':'';?>>Đã thanh toán</option>
												<option value="2" <?php echo $this->input->get('trans_status')=='2'?'selected':'';?>>Thanh toán thất bại</option>
										</select></td>

										<td class="label"><label for="filter_to_date">Đến ngày</label></td>
										<td class="item"><input name="to_date" value="<?php echo $this->input->get('to_date'); ?>" id="filter_to_date" type="date" /></td>

										<td><input type="submit" class="button blueB" value="Lọc" /></td>
									</tr>

								</tbody>
							</table>
						</form>
					</td>
				</tr>
			</thead>

			<thead>
				<tr>
					<td style="width: 60px;">Mã số</td>
					<td>Mã giao dịch</td>
					<td>Sản phẩm</td>
					<td>Số lượng</td>
					<td>Số tiền</td>
					<td>Trạng thái</td>
					<td>Thanh toán</td>
					<td>Ngày tạo</td>
					<td style="width: 120px;">Hành động</td>
				</tr>
			</thead>

			<tfoot class="auto_check_pages">
				<tr>
					<td colspan="9">
						<div class="list_action itemActions">
							<a href="#submit" id="submit" class="button blueB" url="<?php echo admin_url('order/delete_all'); ?>"> <span style='color: white;'>Xóa hết</span>
							</a>
						</div>

						<div class='pagination'><?php echo $this->pagination->create_links(); ?></div>
					</td>
				</tr>
			</tfoot>

			<tbody class="list_item">
				<?php foreach($list as $row): ?>
				<tr class='row_<?php echo $row->id; ?>'>

					<td class="textC"><?php echo $row->id; ?></td>
	
					<td class="textC"><?php echo $row->transaction_id; ?></td>
	
					<td>
						<div class="image_thumb">
							<img src="<?php echo upload_url('product/'.$row->image_link); ?>" height="50">
							<div class="clear"></div>							
						</div>
						<b><?php echo $row->product_name; ?></b>
						<div class="f11">
								<?php if($row->discount): ?>
								<span style="color: red;">Giá đã giảm: <?php echo number_format($row->price-$row->discount); ?> đ&nbsp;|&nbsp;</span>
								<span style="text-decoration: line-through">Giá gốc: <?php echo number_format($row->price); ?> đ</span>
								<?php else: ?>
								<span>Đơn giá: <?php echo number_format($row->price); ?> đ</span>
								<?php endif; ?>
						</div>
					</td>
	
					<td class="textC"><?php echo $row->qty;?></td>
	
					<td class="textR"><p style="color: red;"><?php echo number_format($row->amount); ?> đ</p></td>
	
					<td class="textC">
						<?php if($row->status == 0) echo 'Chờ xử lý'; else if($row->status == 1) echo 'Đã gửi hàng'; else if($row->status == 2) echo 'Hủy'; else echo 'Chưa xác định'; ?>
					</td>
					
					<td class="textC">
						<?php if($row->trans_status == 0) echo 'Chưa thanh toán'; else if($row->trans_status == 1) echo 'Đã thanh toán'; else if($row->trans_status == 2) echo 'Thanh toán thất bại'; else echo 'Chưa xác định'; ?>
					</td>									
					
					<td class="textC"><?php echo get_date($row->created); ?></td>
	
					<td class="option textC">
						<a href="<?php echo ''; ?>" target='_blank' class='tipS' title="Xem hiển thị sản phẩm"> <img src="<?php echo public_url('admin/images'); ?>/icons/color/view.png" /></a>
						<a href="<?php echo admin_url('order/edit/'.$row->id); ?>" title="Chỉnh sửa" class="tipS"> <img src="<?php echo public_url('admin/images'); ?>/icons/color/edit.png" /></a>
						<a href="<?php echo admin_url('order/delete/'.$row->id); ?>" title="Xóa" class="tipS verify_action"> <img src="<?php echo public_url('admin/images'); ?>/icons/color/delete.png" /></a>
					</td>
				</tr>
				<?php endforeach; ?>				
			</tbody>

		</table>
	</div>

</div>
<div class="clear mt30"></div>

<style>
.list_filter table {
	width: 100%;
}

.list_filter table td.label {
	width: 85px;
	height: auto;
}

.list_filter table td.item {
	width: 155px !important;
}

.list_filter table td.item>* {
	width: 100% !important;
}
</style>
