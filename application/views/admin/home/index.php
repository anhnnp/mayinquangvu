<div class="titleArea">
    <div class="wrapper">
        <div class="pageTitle">
            <h5>Bảng điều khiển</h5>
            <span>Trang quản lý hệ thống</span>
        </div>

        <div class="clear"></div>
    </div>
</div>
<div class="line"></div>
<div class="wrapper">

    <div class="widgets">
        <!-- Stats -->

        <!-- Amount -->
        <div class="oneTwo">
            <div class="widget">
                <div class="title">
                    <img src="<?php echo public_url(); ?>/admin/images/icons/dark/money.png" class="titleIcon">
                    <h6>Doanh số</h6>
                </div>

                <table cellpadding="0" cellspacing="0" width="100%" class="sTable myTable">
                    <tbody>

                        <tr>
                            <td class="fontB blue f13">Tổng doanh số</td>
                            <td class="textR webStatsLink red" style="width:120px;">44,000,000 đ</td>
                        </tr>

                        <tr>
                            <td class="fontB blue f13">Doanh số hôm nay</td>
                            <td class="textR webStatsLink red" style="width:120px;">0 đ</td>
                        </tr>

                        <tr>
                            <td class="fontB blue f13">Doanh số theo tháng</td>
                            <td class="textR webStatsLink red" style="width:120px;">
                                0 đ
                            </td>
                        </tr>

                    </tbody>
                </table>
            </div>
        </div>


        <!-- User -->
        <div class="oneTwo">
            <div class="widget">
                <div class="title">
                    <img src="<?php echo public_url(); ?>/admin/images/icons/dark/users.png" class="titleIcon">
                    <h6>Thống kê dữ liệu</h6>
                </div>

                <table cellpadding="0" cellspacing="0" width="100%" class="sTable myTable">
                    <tbody>

                        <tr>
                            <td>
                                <div class="left">Tổng số gia dịch</div>
                                <div class="right f11"><a href="admin/tran.html">Chi tiết</a></div>
                            </td>

                            <td class="textC webStatsLink">
                                15					</td>
                        </tr>

                        <tr>
                            <td>
                                <div class="left">Tổng số sản phẩm</div>
                                <div class="right f11"><a href="admin/product.html">Chi tiết</a></div>
                            </td>

                            <td class="textC webStatsLink">
                                8					</td>
                        </tr>

                        <tr>
                            <td>
                                <div class="left">Tổng số bài viết</div>
                                <div class="right f11"><a href="admin/news.html">Chi tiết</a></div>
                            </td>

                            <td class="textC webStatsLink">
                                4					</td>
                        </tr>

                        <tr>
                            <td>
                                <div class="left">Tổng số thành viên</div>
                                <div class="right f11"><a href="admin/user.html">Chi tiết</a></div>
                            </td>

                            <td class="textC webStatsLink">
                                2					</td>
                        </tr>
                        <tr>
                            <td>
                                <div class="left">Tổng số liên hệ</div>
                                <div class="right f11"><a href="admin/contact.html">Chi tiết</a></div>
                            </td>

                            <td class="textC webStatsLink">
                                0					</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>

        <div class="clear"></div>

        <!-- Giao dich thanh cong gan day nhat -->

        <div class="widget">
            <div class="title">
                <span class="titleIcon"><input type="checkbox" id="titleCheck" name="titleCheck"></span>
                <h6>Danh sách Giao dịch</h6>
            </div>

            <table cellpadding="0" cellspacing="0" width="100%" class="sTable mTable myTable" id="checkAll">


                <thead>
                    <tr>
						<td style="width: 21px;"><img src="<?php echo public_url('admin/images'); ?>/icons/tableArrows.png" /></td>
						<td style="width: 60px;">Mã số</td>
						<td>Người giao dịch</td>
						<td>Số tiền</td>
						<td>Thanh toán</td>
						<td>Trạng thái</td>
						<td style="width: 75px;">Ngày tạo</td>
						<td style="width: 120px;">Hành động</td>
					</tr>
                </thead>

                <tfoot class="auto_check_pages">
                    <tr>
                        <td colspan="8">
                            <div class="list_action itemActions">
                                <a href="#submit" id="submit" class="button blueB" url="admin/tran/del_all.html">
                                    <span style="color:white;">Xóa hết</span>
                                </a>
                            </div>
                        </td>
                    </tr>
                </tfoot>

                <tbody class="list_item">
                    <?php foreach($list as $row): ?>
					<tr class='row_<?php echo $row->id; ?>'>
						<td><input type="checkbox" name="id[]" value="<?php echo $row->id; ?>" /></td>
	
						<td class="textC"><?php echo $row->id; ?></td>
	
						<td><a href="<?php echo admin_url('transaction/'.$row->id); ?>" class="tipS" title="" target="_blank"> <b><?php echo $row->user_name; ?></b></a>
							<div class="f11">Số điện thoại: <?php echo $row->user_phone; ?></div></td>
	
						<td class="textR">
							<p style="color: red;"><?php echo number_format($row->amount); ?> đ</p>
						</td>
	
						<td class="textC"><?php echo $row->payment;?></td>
	
						<td class="textC">
						<?php if($row->status == 0) echo 'Chưa thanh toán'; else if($row->status == 1) echo 'Đã thanh toán'; else if($row->status == 2) echo 'Thanh toán thất bại'; else echo 'Chưa xác định'; ?>
						</td>
	
						<td class="textC"><?php echo get_date($row->created, FALSE); ?></td>
	
						<td class="option textC">
							<a href="<?php echo admin_url('transaction/view/'.$row->id); ?>" class='tipS' title="Xem chi tiết giao dịch"> <img src="<?php echo public_url('admin/images'); ?>/icons/color/view.png" /></a> 
							<a href="<?php echo admin_url('transaction/edit/'.$row->id); ?>" title="Chỉnh sửa" class="tipS"> <img src="<?php echo public_url('admin/images'); ?>/icons/color/edit.png" /></a> 
							<a href="<?php echo admin_url('transaction/delete/'.$row->id); ?>" title="Xóa" class="tipS verify_action"> <img src="<?php echo public_url('admin/images'); ?>/icons/color/delete.png" /></a>
						</td>
					</tr>
					<?php endforeach; ?>	
                </tbody>

            </table>
        </div>

    </div>

</div>

<div class="clear mt30"></div>
