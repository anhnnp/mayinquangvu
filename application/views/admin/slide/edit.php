<?php $this->load->view('admin/slide/header'); ?>
<div class="line"></div>
<div class="wrapper">

	<!-- Form -->
	<form class="form" id="form" action="" method="post" enctype="multipart/form-data">
		<fieldset>
			<div class="widget">
				<div class="title">
					<img src="<?php echo public_url('admin'); ?>/images/icons/dark/add.png" class="titleIcon">
					<h6>Thêm mới slide</h6>
				</div>

				<ul class="tabs">
					<li><a href="#tab1">Thông tin chung</a></li>
				</ul>

				<div class="tab_container">
					<div id="tab1" class="tab_content pd0">
					
						<!-- name -->
						<div class="formRow">
							<label class="formLeft" for="param_name">Tên slide:<span class="req">*</span></label>
							<div class="formRight">
								<span class="oneTwo"><input name="name" id="param_name" _autocheck="true" value="<?php echo $info->name; ?>" type="text"></span> <span name="name_autocheck" class="autocheck"></span>
								<div name="name_error" class="clear error"><?php echo form_error('name'); ?></div>
							</div>
							<div class="clear"></div>
						</div>

						<!-- image_link -->
						<div class="formRow">
							<label class="formLeft">Hình ảnh:<span class="req">*</span></label>
							<div class="formRight">
								<div class="left">
									<input type="file" id="image" name="image">
									<?php if(!empty($info->image_link)): ?>
									<img alt="<?php echo $info->image_link; ?>" src="<?php echo upload_url('slide/'.$info->image_link); ?>" style="width:100px;height:75px;">
									<?php endif; ?>
								</div>
								<div name="image_error" class="clear error"></div>
							</div>
							<div class="clear"></div>
						</div>
						
						<!-- image_name -->
						<div class="formRow">
							<label class="formLeft" for="param_image_name">Tiêu đề ảnh:</label>
							<div class="formRight">
								<span class="oneTwo"><input name="image_name" id="param_image_name" _autocheck="true" value="<?php echo $info->image_name; ?>" type="text"></span> <span name="image_name_autocheck" class="autocheck"></span>
								<div name="image_name_error" class="clear error"><?php echo form_error('image_name'); ?></div>
							</div>
							<div class="clear"></div>
						</div>
						
						<!-- link -->
						<div class="formRow">
							<label class="formLeft" for="param_link">Link liên kết:</label>
							<div class="formRight">
								<span class="oneTwo"><input name="link" id="param_link" _autocheck="true" value="<?php echo $info->link; ?>" type="text"></span> <span name="link_autocheck" class="autocheck"></span>
								<div name="link_error" class="clear error"><?php echo form_error('link'); ?></div>
							</div>
							<div class="clear"></div>
						</div>
						
						<!-- sort_order -->
						<div class="formRow">
							<label class="formLeft" for="param_sort_order">Thứ tự:</label>
							<div class="formRight">
								<span class="oneTwo"><input name="sort_order" id="param_sort_order" _autocheck="true" value="<?php echo $info->sort_order; ?>" type="text"></span> <span name="sort_order_autocheck" class="autocheck"></span>
								<div name="sort_order_error" class="clear error"><?php echo form_error('sort_order'); ?></div>
							</div>
							<div class="clear"></div>
						</div>
						
						<!-- info -->
						<div class="formRow">
							<label class="formLeft" for="param_info">Thông tin mô tả:</label>
							<div class="formRight">
								<span class="oneTwo"><textarea name="info" id="param_info" rows="4" cols=""><?php echo $info->info; ?></textarea></span> <span name="info_autocheck" class="autocheck"></span>
								<div name="info_error" class="clear error"><?php echo form_error('info'); ?></div>
							</div>
							<div class="clear"></div>
						</div>
						
						<div class="formRow hide"></div>
					</div>

				</div>
				<!-- End tab_container-->

				<div class="formSubmit">
					<input type="submit" value="Cập nhật" class="redB">
					<input type="reset" value="Hủy bỏ" class="basic">
				</div>
				<div class="clear"></div>
			</div>
		</fieldset>
	</form>
</div>
