<?php $this->load->view('admin/transaction/header'); ?>
<div class="line"></div>

<div class="wrapper" id="main_transaction">
	<?php $this->load->view('admin/message', $message); ?>
	<div class="widget">
		<div class="title">
			<span class="titleIcon"><input type="checkbox" id="titleCheck" name="titleCheck" /></span>
			<h6>Danh sách giao dịch</h6>
			<div class="num f12">
				Số lượng: <b><?php echo $total; ?></b>
			</div>
		</div>


		<table cellpadding="0" cellspacing="0" width="100%" class="sTable mTable myTable" id="checkAll">

			<thead class="filter">
				<tr>
					<td colspan="8">
						<form class="list_filter form" action="<?php echo admin_url('transaction'); ?>" method="get">
							<table>
								<tbody>

									<tr>
										<td class="label"><label for="filter_id">Mã số</label></td>
										<td class="item"><input name="id" value="<?php echo $this->input->get('id'); ?>" id="filter_id" type="text" /></td>

										<td class="label"><label for="filter_payment_method">Thanh toán</label></td>
										<td class="item"><select name="payment_method" id="filter_payment_method">
												<option value=""></option>
												<option value="offline" <?php echo $this->input->get('payment_method')=='offline'?'selected':'';?>>Nhận hàng trả tiền</option>
												<option value="baokim" <?php echo $this->input->get('payment_method')=='baokim'?'selected':'';?>>Bảo kim</option>
												<option value="nganluong" <?php echo $this->input->get('payment_method')=='nganluong'?'selected':'';?>>Ngân lượng</option>
										</select></td>

										<td class="label"><label for="filter_from_date">Từ ngày</label></td>
										<td class="item"><input name="from_date" value="<?php echo $this->input->get('from_date'); ?>" id="filter_form_date" type="date" /></td>

										<td><input type="reset" class="basic" value="Reset" onclick="window.location.href = '<?php echo admin_url('transaction'); ?>'; "></td>

									</tr>

									<tr>
										<td class="label"><label for="filter_user">Thành viên</label></td>
										<td class="item"><input name="user" value="<?php echo $this->input->get('user'); ?>" id="filter_user" type="text" /></td>

										<td class="label"><label for="filter_trans_status">Trạng thái</label></td>
										<td class="item"><select name="trans_status" id="filter_trans_status">
												<option value=""></option>
												<option value="0" <?php echo $this->input->get('trans_status')=='0'?'selected':'';?>>Chưa thanh toán</option>
												<option value="1" <?php echo $this->input->get('trans_status')=='1'?'selected':'';?>>Đã thanh toán</option>
												<option value="2" <?php echo $this->input->get('trans_status')=='2'?'selected':'';?>>Thanh toán thất bại</option>
										</select></td>

										<td class="label"><label for="filter_to_date">Đến ngày</label></td>
										<td class="item"><input name="to_date" value="<?php echo $this->input->get('to_date'); ?>" id="filter_to_date" type="date" /></td>

										<td><input type="submit" class="button blueB" value="Lọc" /></td>
									</tr>

								</tbody>
							</table>
						</form>
					</td>
				</tr>
			</thead>

			<thead>
				<tr>
					<td style="width: 21px;"><img src="<?php echo public_url('admin/images'); ?>/icons/tableArrows.png" /></td>
					<td style="width: 60px;">Mã số</td>
					<td>Người giao dịch</td>
					<td>Số tiền</td>
					<td>Thanh toán</td>
					<td>Trạng thái</td>
					<td style="width: 75px;">Ngày tạo</td>
					<td style="width: 120px;">Hành động</td>
				</tr>
			</thead>

			<tfoot class="auto_check_pages">
				<tr>
					<td colspan="8">
						<div class="list_action itemActions">
							<a href="#submit" id="submit" class="button blueB" url="<?php echo admin_url('transaction/delete_all'); ?>"> <span style='color: white;'>Xóa hết</span>
							</a>
						</div>

						<div class='pagination'><?php echo $this->pagination->create_links(); ?></div>
					</td>
				</tr>
			</tfoot>

			<tbody class="list_item">
				<?php foreach($list as $row): ?>
				<tr class='row_<?php echo $row->id; ?>'>
					<td><input type="checkbox" name="id[]" value="<?php echo $row->id; ?>" /></td>

					<td class="textC"><?php echo $row->id; ?></td>

					<td><a href="<?php echo admin_url('transaction/'.$row->id); ?>" class="tipS" title="" target="_blank"> <b><?php echo $row->user_name; ?></b></a>
						<div class="f11">Số điện thoại: <?php echo $row->user_phone; ?></div></td>

					<td class="textR">
						<p style="color: red;"><?php echo number_format($row->amount); ?> đ</p>
					</td>

					<td class="textC"><?php echo $row->payment;?></td>

					<td class="textC">
					<?php if($row->status == 0) echo 'Chưa thanh toán'; else if($row->status == 1) echo 'Đã thanh toán'; else if($row->status == 2) echo 'Thanh toán thất bại'; else echo 'Chưa xác định'; ?>
					</td>

					<td class="textC"><?php echo get_date($row->created, FALSE); ?></td>

					<td class="option textC">
						<a href="<?php echo admin_url('transaction/view/'.$row->id); ?>" class='tipS' title="Xem chi tiết giao dịch"> <img src="<?php echo public_url('admin/images'); ?>/icons/color/view.png" /></a> 
						<a href="<?php echo admin_url('transaction/edit/'.$row->id); ?>" title="Chỉnh sửa" class="tipS"> <img src="<?php echo public_url('admin/images'); ?>/icons/color/edit.png" /></a> 
						<a href="<?php echo admin_url('transaction/delete/'.$row->id); ?>" title="Xóa" class="tipS verify_action"> <img src="<?php echo public_url('admin/images'); ?>/icons/color/delete.png" /></a>
					</td>
				</tr>
				<?php endforeach; ?>				
			</tbody>

		</table>
	</div>

</div>
<div class="clear mt30"></div>

<style>
	.list_filter table {
		width: 100%;
	}
	
	.list_filter table td.label {
		width: 85px;
		height: auto;
	}
	
	.list_filter table td.item {
		width: 155px !important;
	}
	
	.list_filter table td.item>* {
		width: 100% !important;
	}
</style>
