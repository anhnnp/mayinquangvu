<div class="titleArea">
	<div class="wrapper">
		<div class="pageTitle">
			<h5>Giao dịch</h5>
			<span>Quản lý giao dịch</span>
		</div>

		<div class="horControlB menu_action">
			<ul>
				<li><a href="<?php echo admin_url('order'); ?>"> <img src="<?php echo public_url('admin/images'); ?>/icons/control/16/add.png" /> <span>Đơn hàng</span>
				</a></li>

				<li><a href="admin/transaction/?feature=1.html"> <img src="<?php echo public_url('admin/images'); ?>/icons/control/16/feature.png" /> <span>Tiêu biểu</span>
				</a></li>

				<li><a href="<?php echo admin_url('transaction'); ?>"> <img src="<?php echo public_url('admin/images'); ?>/icons/control/16/list.png" /> <span>Giao dịch</span>
				</a></li>

			</ul>
		</div>

		<div class="clear"></div>
	</div>
</div>
<script type="text/javascript">
(function($)
{
	$(document).ready(function()
	{
		var main = $('#main_transaction');
		
		// Tips
		main.find('.tipN').tipsy({gravity:'n', fade:false, html:true});
		main.find('.tipS').tipsy({gravity:'s', fade:false, html:true});
		main.find('.tipW').tipsy({gravity:'w', fade:false, html:true});
		main.find('.tipE').tipsy({gravity:'e', fade:false, html:true});
		
		// Tooltip
// 		main.find('[_tooltip]').nstUI({
// 			method:	'tooltip'
// 		});
	});
})(jQuery);
</script>

<script type="text/javascript">
(function($)
{
	$(document).ready(function()
	{
		var main = $('#form');
		
		// Tabs
		main.contentTabs();
	});
})(jQuery);
</script>