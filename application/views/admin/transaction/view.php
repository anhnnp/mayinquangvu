<?php $this->load->view('admin/transaction/header'); ?>
<div class="line"></div>

<style>
	.myTable .label {
		text-align: left;
		width: 20%;
		font-size: 1em;
		padding: 10px;
	}
	.myTable .item {
		text-align: left;
		width: 80%;
		font-size: 1em;
		padding: 10px;
		color: black;
	}
</style>

<div class="wrapper" id="main_transaction">
	<?php $this->load->view('admin/message', $message); ?>
	<div class="widget">		
		<table width="100%" class="sTable mTable myTable">
			<thead>
				<tr>
					<td class="label">Người mua hàng:</td>
					<td class="item"><?php echo $transaction->user_name; ?></td>
				</tr>
				<tr>
					<td class="label">Địa chỉ hòm thư:</td>
					<td class="item"><?php echo $transaction->user_email; ?></td>
				</tr>
				<tr>
					<td class="label">Số điện thoại:</td>
					<td class="item"><?php echo $transaction->user_phone; ?></td>
				</tr>
				<tr>
					<td class="label">Phương thức thanh toán:</td>
					<td class="item"><?php echo $transaction->payment; ?></td>
				</tr>
				<tr>
					<td class="label">Tình trạng thanh toán:</td>
					<td class="item"><?php echo $transaction->status==1?'Đã thanh toán':($transaction->status==2?'Thanh toán thất bại':($transaction->status==0?'Chưa thanh toán':'Không xác định')); ?></td>
				</tr>
				<tr>
					<td class="label">Ngày tạo giao dịch:</td>
					<td class="item"><?php echo get_date($transaction->created); ?></td>
				</tr>
				<tr>
					<td class="label">Ghi chú thêm:</td>
					<td class="item"><?php echo $transaction->message; ?></td>
				</tr>
				<tr>
					<td class="label">Tổng giá trị đơn hàng:</td>
					<td class="item" style="color:red;font-weight:600;"><?php echo number_format($transaction->amount); ?> VNĐ</td>
				</tr>
			</thead>

			<tfoot>
				<tr>
					<td colspan="2">
						<div class="list_action itemActions">
							<a href="<?php echo admin_url('transaction/edit/'.$transaction->id)?>" class="button blueB"> <span style='color: white;'>Chỉnh sửa</span></a>
						</div>
					</td>
				</tr>
			</tfoot>

			<tbody>
				<tr>
					<td colspan="2">
						<div class="title">
							<h6>Danh sách đơn hàng</h6>
							<div class="num f12">
								Số lượng: <b><?php echo $total; ?></b>
							</div>
						</div>
						<table width="100%">
							<thead>
								<tr>
									<td style="width: 60px;">Mã số</td>
									<td>Sản phẩm</td>
									<td>Đơn giá</td>
									<td>Số lượng</td>
									<td>Số tiền</td>
									<td>Trạng thái</td>
									<td style="width: 120px;">Hành động</td>
								</tr>
							</thead>

							<tbody class="list_item">
								<?php if(!empty($list_products)): ?>
								<?php foreach($list_products as $row): ?>
								<tr class='row_<?php echo $row->id; ?>'>
								
									<td class="textC"><?php echo $row->id; ?></td>

									<td><div class="image_thumb">
											<img src="<?php echo upload_url('product/'.$row->image_link); ?>" height="50">
											<div class="clear"></div>
										</div>
										<b><?php echo $row->product_name; ?></b>
									</td>

									<td class="textR">
										<?php if($row->discount): ?>
										<p style="color: red;"><?php echo number_format($row->price-$row->discount); ?> đ</p>
										<p style="text-decoration: line-through"><?php echo number_format($row->price); ?> đ</p>
										<?php else: ?>
										<p><?php echo number_format($row->price); ?> đ</p>
										<?php endif; ?>
									</td>

									<td class="textC"><?php echo $row->qty;?></td>

									<td class="textR">
										<p style="color: red;"><?php echo number_format($row->amount); ?> đ</p>
									</td>

									<td class="textC">
									<?php if($row->status == 0) echo 'Chờ xử lý'; else if($row->status == 1) echo 'Đã gửi hàng'; else if($row->status == 2) echo 'Hủy'; else echo 'Chưa xác định'; ?>
									</td>

									<td class="option textC">
									
									</td>
								</tr>
								<?php endforeach; ?>
								<?php endif; ?>				
							</tbody>
						</table>

					</td>
				</tr>
			</tbody>
		</table>
	</div>
</div>
<div class="clear mt30"></div>