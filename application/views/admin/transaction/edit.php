<?php $this->load->view('admin/transaction/header'); ?>
<div class="line"></div>

<style>
	.myTable .label {
		text-align: left;
		width: 20%;
		font-size: 1em;
		padding: 10px;
	}
	.myTable .item {
		text-align: left;
		width: 80%;
		font-size: 1em;
		padding: 10px;
		color: black;
	}
</style>

<div class="wrapper" id="main_transaction">
	<?php $this->load->view('admin/message', $message); ?>
	<div class="widget">		
		<table width="100%" class="sTable mTable myTable">
			<thead>
				<tr>
					<td class="label"><label for="param_user_name">Người mua hàng:</label></td>
					<td class="item"><input value="<?php echo $transaction->user_name; ?>" type="text" name="user_name" id="param_user_name"></td>
				</tr>
				<tr>
					<td class="label"><label for="param_user_email">Địa chỉ hòm thư:</label></td>
					<td class="item"><input value="<?php echo $transaction->user_email; ?>" type="text" name="user_name" id="param_user_name"></td>
				</tr>
				<tr>
					<td class="label"><label for="param_user_phone">Số điện thoại:</label></td>
					<td class="item"><input value="<?php echo $transaction->user_phone; ?>" type="text" name="user_name" id="param_user_name"></td>
				</tr>
				<tr>
					<td class="label"><label for="param_payment">Phương thức thanh toán:</label></td>
					<td class="item"><input value="<?php echo $transaction->payment; ?>" type="text" name="user_name" id="param_user_name"></td>
				</tr>
				<tr>
					<td class="label"><label for="param_status">Tình trạng thanh toán:</label></td>
					<td class="item">
						<select>
							<option value="0" <?php echo $transaction->status==0?'selected':'';?>>Chưa thanh toán</option>
							<option value="1" <?php $transaction->status==1?'selected':''?>>Đã thanh toán</option>
							<option value="2" <?php echo $transaction->status==2?'selected':'';?>>Thanh toán thất bại</option>
						</select>
					</td>
				</tr>
				<tr>
					<td class="label"><label for="param_created">Ngày tạo giao dịch:</label></td>
					<td class="item"><input value="<?php echo get_date($transaction->created); ?>" type="text" name="user_name" id="param_user_name"></td>
				</tr>
				<tr>
					<td class="label"><label for="param_message">Ghi chú thêm:</label></td>
					<td class="item"><textarea><?php echo $transaction->message; ?></textarea></td>
				</tr>
				<tr>
					<td class="label">Tổng giá trị đơn hàng:</td>
					<td class="item" style="color:red;font-weight:600;"><?php echo number_format($transaction->amount); ?> VNĐ</td>
				</tr>
			</thead>

			<tfoot>
				<tr>
					<td colspan="2">
						<div class="list_action itemActions">
							<a href="<?php echo admin_url('transaction/edit')?>" class="button blueB"> <span style='color: white;'>Cập nhật</span></a>
						</div>
					</td>
				</tr>
			</tfoot>

			<tbody>
				<tr>
					<td colspan="2">
						<div class="title">
							<h6>Danh sách đơn hàng</h6>
							<div class="num f12">
								Số lượng: <b><?php echo $total; ?></b>
							</div>
						</div>
						<table width="100%">
							<thead>
								<tr>
									<td style="width: 60px;">Mã số</td>
									<td>Sản phẩm</td>
									<td>Đơn giá</td>
									<td>Số lượng</td>
									<td>Số tiền</td>
									<td>Trạng thái</td>
									<td style="width: 120px;">Hành động</td>
								</tr>
							</thead>

							<tbody class="list_item">
								<?php if(!empty($list_products)): ?>
								<?php foreach($list_products as $row): ?>
								<tr class='row_<?php echo $row->id; ?>'>
								
									<td class="textC"><?php echo $row->id; ?></td>

									<td><div class="image_thumb">
											<img src="<?php echo upload_url('product/'.$row->image_link); ?>" height="50">
											<div class="clear"></div>
										</div>
										<b><?php echo $row->product_name; ?></b>
									</td>

									<td class="textR">
										<?php if($row->discount): ?>
										<p style="color: red;"><?php echo number_format($row->price-$row->discount); ?> đ</p>
										<p style="text-decoration: line-through"><?php echo number_format($row->price); ?> đ</p>
										<?php else: ?>
										<p><?php echo number_format($row->price); ?> đ</p>
										<?php endif; ?>
									</td>

									<td class="textC"><input value="<?php echo $row->qty;?>" type="text" name="qty"></td>

									<td class="textR">
										<p style="color: red;"><?php echo number_format($row->amount); ?> đ</p>
									</td>

									<td class="textC">
										<select>
											<option value="0" <?php echo $row->status==0?'selected':'';?>>Chờ xử lý</option>
											<option value="1" <?php $row->status==1?'selected':''?>>Đã gửi hàng</option>
											<option value="2" <?php echo $row->status==2?'selected':'';?>>Hủy</option>
										</select>
									</td>

									<td class="option textC">
									<a href="<?php echo admin_url('transaction/delete/'.$row->id); ?>" title="Xóa" class="tipS verify_action"> <img src="<?php echo public_url('admin/images'); ?>/icons/color/delete.png" /></a></td>
								</tr>
								<?php endforeach; ?>
								<?php endif; ?>				
							</tbody>
						</table>

					</td>
				</tr>
			</tbody>
		</table>
	</div>
</div>
<div class="clear mt30"></div>